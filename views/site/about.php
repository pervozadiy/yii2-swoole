<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(<<<JS
var webSocket = new WebSocket('ws://localhost:2180');
webSocket.onopen = function(event) {
    // console.log(event);
    var payload = {event: 'MESSAGE', data: {text: 'ogo I pishu'}};
    webSocket.send(JSON.stringify(payload));
}
webSocket.onmessage = function(event) {
  console.log(event)
}
JS
);
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= Yii::$app->cache->get('pidor') ?></code>
</div>
