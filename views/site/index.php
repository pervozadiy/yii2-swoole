<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$this->registerJsFile('https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js');
$this->registerJs(<<<JS
var socket = io('/');

var socket2 = io('/nsp');

socket.on('connect', function(){
    console.log('CONNECTED');
    socket.emit('eeeeee');
    socket.emit('eeeeee');
    socket.emit('eeeeee');
    socket.emit('eeeeee');
    
});
socket.on('news', function(data){
    console.log(data)
});
socket2.on('connect', function(){
    console.log('CONNECTED2');
    socket2.emit('eeeeee');
});
socket2.on('news', function(data){
    console.log(data)
});
socket.on('disconnect', function(){});
// var webSocket = new WebSocket('ws://localhost:2180/namespace/lol');
// webSocket.onopen = function(event) {
//     // console.log(event);
//     var payload = {event: 'JOIN_ROOM'};
//     webSocket.send(JSON.stringify(payload));
// };
// webSocket.onmessage = function(event) {
//    console.log(event)
// }
JS
);
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
