<?php


$serv = new swoole_http_server('0.0.0.0', 8101);
$serv->set(array(
    'worker_num' => 1,
    'task_worker_num' => 1,
    'daemonize' => 0,
));
$serv->on('Request', function (\Swoole\Http\Request $req, \Swoole\Http\Response $resp) use ($serv)
{
    $data = str_repeat('A', 8192 * 10);

    $serv->task($data);
    $s = microtime(true);
    $mysql = new \Swoole\Coroutine\MySQL();
    $mysql->connect([
        'host' => 'master',
        'user' => 'root',
        'password' => 'verysecret',
        'database' => 'yii2basic'
    ]);
    $statement = $mysql->prepare('SELECT * FROM `user` LIMIT 1000');
    $statement2 = $mysql->prepare('SELECT * FROM `user` LIMIT 2000');
    $statement3 = $mysql->prepare('SELECT * FROM `user` LIMIT 3000');
    $statement4 = $mysql->prepare('SELECT * FROM `user` LIMIT 4000');
    for ($n = 100; $n--;) {
        $result = $statement->execute();
        $statement4->execute();
        $statement2->execute();
        $statement3->execute();
        assert(count($result) > 0);
    }
    echo 'use ' . (microtime(true) - $s) . ' s';
    $resp->end(<<<HTML
<a href="#">CLone page</a>
<script src="https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js"></script>
<script>
fetch(window.location.href, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        body: JSON.stringify({'swoole': 'hueta'}), // тип данных в body должен соответвовать значению заголовка "Content-Type"
    })
    .then(response => response.json())
    .then(json => {console.log(json)})
</script>
HTML
        . PHP_EOL);

});
$serv->on('Task', function (swoole_server $serv, $task_id, $reactor_id, $data) {
    //echo "#{$serv->worker_id}\tonTask: [PID={$serv->worker_pid}]: task_id=$task_id, data_len=".strlen($data).".".PHP_EOL;
    $serv->finish($data);
//    return $data;
});
$serv->on('Finish', function (swoole_server $serv, $task_id, $data) {

    echo "Task#$task_id finished, data_len=".strlen($data).PHP_EOL;
});
$serv->on('workerStart', function(swoole_server $serv, $worker_id) {
    global $argv;
    if ($serv->taskworker)
    {
        swoole_set_process_name("php {$argv[0]}: task_worker");
    }
    else
    {
        swoole_set_process_name("php {$argv[0]}: worker");
    }
});
$serv->on('workerStop', function (swoole_server $serv, $id) {
    echo "stop\n";
    var_dump($id);
});
$serv->start();