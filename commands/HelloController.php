<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{

    public function actionIndex()
    {
        $faker = \Faker\Factory::create();

        $user = new User();
        for ( $i = 1; $i <= 2000000; $i++ )
        {
            if ($i % 1000 === 0) {
                echo $i . PHP_EOL;
            }
            $user->username = uniqid($faker->userName, true);
            $user->email = uniqid($faker->email, true);
            $user->password_hash = \Yii::$app->security->generateRandomString(16);
            $user->auth_key = \Yii::$app->security->generateRandomString(16);
            try {
                $user->insert(false);
            } catch (\Throwable $e) {
                var_dump($e->getTraceAsString());
                $this->stderr($e->getMessage(), Console::FG_RED);
                continue;
            }

        }

    }
}
