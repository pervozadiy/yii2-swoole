<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$web_params = require 'web.php';
$rootDir = dirname(__DIR__);

$config = [
    'id' => 'basic-console',
    'basePath' => $rootDir,
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/console_info.log',
                    'logVars' => [],
                    'levels' => ['info'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/console_error.log',
                    'logVars' => [],
                    'levels' => ['error'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/console_warning.log',
                    'logVars' => [],
                    'levels' => ['warning'],
                ],
            ],
            'traceLevel' => 5
        ],
        'db' => $db,
    ],
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
            'templatePath' => dirname(__DIR__) . '/vendor/yiisoft/yii2-faker/tests/data/templates',
            'namespace' => 'common\ActiveRecords'
        ],
    ],
    'modules' => [
        'swoole' => [
            'class' => \app\modules\swoole\Module::class,
            'rootDir' => dirname(__DIR__),
            'bootstrappers' => [
                [
                    'class' => \app\modules\swoole\bootstrap\WebBootstrapper::class,
                    'appConfig' => require __DIR__ . '/web.php',
                    'webRoot' => $rootDir . '/web',
                ],
//                [
//                    'class' => \app\modules\swoole\bootstrap\HmrBootstrapper::class,
////                    'watchInterval' => 10
//                ],
                [
                    'class' => \app\modules\swoole\bootstrap\TaskBootstrapper::class
                ],
                [
                    'class' => \app\modules\swoole\bootstrap\WebSocketBootsrapper::class,
                    'IOConfig' => require __DIR__ . '/websocket.php',
                ]
            ],
            'config' => [
                'port' => 8101,
                'mode' => SWOOLE_PROCESS,
                'settings' => [
                    'reactor_num' => 1,
                    'worker_num' => 1,
                    'task_worker_num' => 1,
                    'daemonize' => 0,
                    'log_file' => $rootDir . '/runtime/logs/swoole.log',
                    'log_level' => SWOOLE_LOG_DEBUG,
                    'pid_file' => $rootDir . '/runtime/server.pid',
                    'document_root' => $rootDir . '/web',
                    'enable_static_handler' => true,
                ],
                'websocket' => [
                    'enabled' => true
                ]
            ]
        ]
    ],
    'params' => $params,
];

return $config;
