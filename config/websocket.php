<?php

$params = require __DIR__ . '/params.php';

return [
    'pingInterval' => 25000,
    'pingTimeout' => 60000,
    'path' => '/socket.io'
];