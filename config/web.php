<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'secret-key',
//            'enableCookieValidation' => false,
//            'enableCsrfValidation' => false,
            'enableCsrfCookie' => false,
        ],
        'queue' => [
            'class' => \app\modules\swoole\task\SwooleQueue::class,
            'tableSize' => 2048
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/web_info.log',
                    'logVars' => [],
                    'levels' => ['info'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/web_error.log',
                    'logVars' => [],
                    'levels' => ['error'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/web_warning.log',
                    'logVars' => [],
                    'levels' => ['warning'],
                ],
            ],
        ],
        'session' => [
            'class' => \feehi\web\Session::class,
            'savePath' => __DIR__ . '/../runtime/session'

        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
//                [
//                    'pattern' => 'socket',
//                    'route' => 'websocket/index',
//                    'suffix' => '.io',
//                ]
//                'socket.io' => 'websocket/index'
            ],
        ],
        'errorHandler' => [
            'class' => 'deepziyu\yii\swoole\web\ErrorHandler'
        ],
        'cache' => [
//            'class' => \app\modules\swoole\cache\SwooleCache::class,
//            'class' => \yii\caching\FileCache::class,
            'class' => \app\modules\swoole\cache\FileCache::class
        ],
    ],
    'params' => $params,
//    'on beforeRequest' => function (\yii\base\Event $event) {
//        $app = $event->sender;
//        $app->getDb()->initSchema();
//    }
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => \app\modules\swoole\debug\Module::class,
//        // uncomment the following to add your IP if you are not connecting from localhost.
//        'allowedIPs' => ['*'],
//    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
