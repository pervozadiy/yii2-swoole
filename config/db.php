<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=master;dbname=yii2basic',
    'username' => 'hushchyn',
    'password' => 'secret',
    'charset' => 'utf8mb4',
//    'slaves' => [
//        [
//            'dsn' => 'mysql:host=slave;dbname=yii2basic',
//            'username' => 'hushchyn',
//            'password' => 'secret',
//            'charset' => 'utf8mb4',
//        ]
//    ]


    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
