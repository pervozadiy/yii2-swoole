<?php

namespace app\modules\swoole\server;

use yii\base\Model;

class WebsocketConfig extends Model
{
    public $enabled;

    public function rules()
    {
        return [
            ['enabled', 'boolean']
        ];
    }

    public function formName()
    {
        return 'websocket';
    }

    public function __construct(array $rawConfig, array $config = [])
    {
        parent::__construct($config);

        $this->load($rawConfig);
    }
}
