<?php

declare(strict_types=1);

namespace app\modules\swoole\server;

use yii\base\Model;

class SocketConfig extends Model
{
    public const AVAILABLE_MODES = [
        SWOOLE_BASE,
        SWOOLE_PROCESS
    ];
    public const AVAILABLE_TYPES = [
        SWOOLE_SOCK_TCP,
        SWOOLE_SOCK_TCP6,
        SWOOLE_SOCK_UDP,
        SWOOLE_SOCK_UDP6,
        SWOOLE_SOCK_UNIX_DGRAM,
        SWOOLE_SOCK_UNIX_STREAM,
    ];

    /**
     * @var string
     */
    public $host = '0.0.0.0';

    public $port = 9501;
    /**
     * @var int swoole's process mode
     */
    public $mode = SWOOLE_PROCESS;
    /**
     * @var int swoole socket
     */
    public $type = SWOOLE_SOCK_TCP;

    public function __construct(array $rawConfig, array $config = [])
    {
        parent::__construct($config);

        $this->load($rawConfig);
    }

    /**
     * {@inheritDoc
     */
    public function rules()
    {
        return [
            ['host', 'ip'],
            ['port', 'integer'],
            ['mode', 'in', 'range' => self::AVAILABLE_MODES],
            ['type', 'in', 'range' => self::AVAILABLE_TYPES]
        ];
    }

    /**
     * {@inheritDoc
     */
    public function formName()
    {
        return '';
    }

    public function getConfigArray(): array
    {
        return $this->attributes;
    }
}
