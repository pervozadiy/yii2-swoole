<?php

namespace app\modules\swoole\server;

use app\modules\swoole\storage\SwooleChannelStorage;
use app\modules\swoole\storage\SwooleTableStorage;

trait WithStorages
{
    /**
     * @var SwooleTableStorage
     */
    protected $_tableStorage;

    /**
     * @var SwooleChannelStorage
     */
    protected $_channelStorage;

    public function getTableStorage(): SwooleTableStorage
    {
        if ($this->_tableStorage === null) {
            $this->_tableStorage = new SwooleTableStorage();
        }
        return $this->_tableStorage;
    }

    public function getChannelStorage(): SwooleChannelStorage
    {
        if ($this->_channelStorage === null) {
            $this->_channelStorage = new SwooleChannelStorage();
        }
        return $this->_channelStorage;
    }
}
