<?php

declare(strict_types=1);

namespace app\modules\swoole\server;

use Swoole\Http\Server;
use yii\base\InvalidConfigException;

class Factory
{
    /**
     * @param SocketConfig $socketConfig
     * @param WebsocketConfig $wsConfig
     * @return Server
     * @throws InvalidConfigException
     */
    public static function create(SocketConfig $socketConfig, WebsocketConfig $wsConfig): Server
    {
        if (!$socketConfig->validate()) {
            throw new InvalidConfigException(implode('; ' . PHP_EOL, $socketConfig->firstErrors));
        }
        if (!$wsConfig->validate()) {
            throw new InvalidConfigException(implode('; ' . PHP_EOL, $wsConfig->firstErrors));
        }
        $class = $wsConfig->enabled ? \Swoole\Websocket\Server::class : Server::class;
        $server = new $class($socketConfig->host, $socketConfig->port, $socketConfig->mode, $socketConfig->type);

        return $server;
    }
}
