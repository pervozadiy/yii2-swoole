<?php

namespace app\modules\swoole\server;

use app\modules\swoole\coroutine\Context;
use app\modules\swoole\di\ContextContainer;
use app\modules\swoole\server\events\FinishEvent;
use app\modules\swoole\server\events\ManagerStartEvent;
use app\modules\swoole\server\events\RequestEvent;
use app\modules\swoole\server\events\ServerEvent;
use app\modules\swoole\server\events\ShutdownEvent;
use app\modules\swoole\server\events\StartEvent;
use app\modules\swoole\server\events\TaskEvent;
use app\modules\swoole\server\events\WorkerStartEvent;
use app\modules\swoole\server\events\WorkerStopEvent;
use app\modules\swoole\web\NullApp;
use Swoole\Http\Server as HttpServer;
use Swoole\Process;
use Swoole\Runtime;
use Swoole\Server;
use Swoole\Websocket\Server as WebsocketServer;
use Yii;
use yii\base\Component;
use yii\base\InvalidCallException;
use yii\web\ErrorHandler;
use function array_merge;
use function in_array;

/**
 *
 * @property null|Server|HttpServer|WebsocketServer $server
 * @property mixed $processTitle
 */
class Manager extends Component
{
    use WithStorages;

    public const EVENT_WORKER_START = 'WorkerStart';
    public const EVENT_WORKER_STOP = 'WorkerStop';
    public const EVENT_WORKER_ERROR = 'WorkerError';
    public const EVENT_START = 'Start';
    public const EVENT_SHUTDOWN = 'Shutdown';
    public const EVENT_REQUEST = 'Request';
    public const EVENT_TASK = 'Task';
    public const EVENT_FINISH = 'Finish';
    public const EVENT_MANAGER_STOP = 'ManagerStop';
    public const EVENT_MANAGER_START = 'ManagerStart';

    /**
     * @var string ID of application
     */
    public $id;

    protected $configuration;

    protected $running = false;

    /**
     * @var Server|Manager|WebsocketServer
     */
    protected $_server;

    /**
     * Server events.
     *
     * @var array
     */
    protected $events = [
        self::EVENT_START,
        self::EVENT_SHUTDOWN,
        self::EVENT_WORKER_START,
        self::EVENT_WORKER_STOP,
        self::EVENT_WORKER_ERROR,
        'packet',
        'bufferFull',
        'bufferEmpty',
        self::EVENT_TASK,
        self::EVENT_FINISH,
        'pipeMessage',
        self::EVENT_MANAGER_START,
        self::EVENT_MANAGER_STOP,
        self::EVENT_REQUEST,
    ];

    protected $eventsClassMap = [
        self::EVENT_START => StartEvent::class,
        self::EVENT_SHUTDOWN => ShutdownEvent::class,
        self::EVENT_WORKER_START => WorkerStartEvent::class,
        self::EVENT_WORKER_STOP => WorkerStopEvent::class,
        self::EVENT_TASK => TaskEvent::class,
        self::EVENT_FINISH => FinishEvent::class,
        self::EVENT_MANAGER_START => ManagerStartEvent::class,
        self::EVENT_REQUEST => RequestEvent::class,
    ];

    public function __construct(ServerConfig $configuration, array $config = [])
    {
        $this->configuration = $configuration;
        $this->running = false;

        parent::__construct($config);
    }

    public function addEvents(array $events): self
    {
        $this->events = array_merge($this->events, $events);

        return $this;
    }

    public function addEvent(string $event): self
    {
        if (in_array($event, $this->events, true)) {
            $this->events[] = $event;
        }
        return $this;
    }

    public function setEvents(array $events): self
    {
        $this->events = $events;

        return $this;
    }

    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->id = $this->id ?: uniqid('swoole_server_', true);
    }

    /**
     * @return bool
     */
    public function start(): bool
    {
        return $this->running = $this->_server->start();
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */
    public function shutdown(): bool
    {
        if ($this->_server instanceof Server) {
            $this->running = !$this->_server->shutdown();
        } elseif ($this->isRunningInBackground()) {
            $this->running = !Process::kill($this->configuration->getPid(), SIGTERM);
        } else {
            throw new \RuntimeException('Swoole HTTP Server is not running.');
        }
        return !$this->running;
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */
    public function reload(): bool
    {
        if ($this->_server instanceof Server) {
            $this->running = $this->_server->reload();
        } elseif ($this->isRunningInBackground()) {
            /** @link https://www.swoole.co.uk/docs/modules/swoole-server-methods#swoole_server-reload */
            $this->running = Process::kill($this->configuration->getPid(), SIGUSR1);
        } else {
            throw new \RuntimeException('Swoole HTTP Server is not running.');
        }

        return $this->running;
    }

    /**
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->running || $this->isRunningInBackground();
    }

    /**
     * @return bool
     */
    private function isRunningInBackground(): bool
    {
        try {
            return Process::kill($this->configuration->getPid(), SIG_DFL);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * @param Server|Manager|WebsocketServer $server
     * @return Manager
     */
    public function attach(Server $server): self
    {
        if ($this->_server !== null) {
            throw new InvalidCallException('Server is already attached!');
        }
        $this->_server = $server;
        $this->setSwooleServerListeners();

        return $this;
    }

    /**
     * @return Server|Manager|WebsocketServer|null
     */
    public function getServer(): ?Server
    {
        return $this->_server;
    }

    /**
     * Set swoole server listeners.
     */
    protected function setSwooleServerListeners(): void
    {
        foreach ($this->events as $event) {
            $callback = $this->getEventListener($event);

            $this->_server->on($event, $callback);
        }
    }

    public function getEventListener(string $event): callable
    {
        $event = ucfirst($event);
        $listener = 'on' . $event;

        return method_exists($this, $listener) ? [$this, $listener] : function (...$args) use ($event) {
            /** @var ServerEvent $eventObj */
            $eventObj = Yii::createObject(['class' => $this->eventsClassMap[$event] ?? ServerEvent::class]);
            $eventObj->mapArgsToProps($args);
            $this->trigger($event, $eventObj);
        };
    }

    /**
     * @param Server $server
     * @see https://wiki.swoole.com/wiki/page/p-event/onStart.html
     */
    public function onStart(Server $server): void
    {
        $this->setProcessTitle('master');
        $this->trigger(self::EVENT_START, new StartEvent([
            'server' => $server
        ]));
    }

    public function onManagerStart(Server $server): void
    {
        $this->setProcessTitle('manager');
        $this->trigger(self::EVENT_MANAGER_START, new ManagerStartEvent(['server' => $server]));
    }

    /**
     * set process title
     * @param $channelName
     */
    private function setProcessTitle($channelName): void
    {
        //@see https://wiki.swoole.com/wiki/page/125.html
        @swoole_set_process_name("php {$this->id} :$channelName swoole process");
    }

    /**
     * @param Server $server
     * @param $worker_id
     */
    public function onWorkerStart(Server $server, $worker_id): void
    {
        if ($server->taskworker) {
            $this->setProcessTitle('task');
        } else {
            Runtime::enableCoroutine(true);
            $this->setProcessTitle('worker');
            try {
                Yii::$context = Context::getInstance();
                Yii::$container = new ContextContainer();
                Yii::$app = new NullApp($this);
            } catch (\Exception $e) {
                print_r('start yii error:' . ErrorHandler::convertExceptionToString($e) . PHP_EOL);
                $this->server->shutdown();
                exit(1);
            }
        }
        $this->trigger(self::EVENT_WORKER_START, new WorkerStartEvent([
            'server' => $server,
            'workerId' => $worker_id
        ]));
    }


    public function onWorkerError(Server $server, int $worker_id, int $worker_pid, int $exit_code, int $signal): void
    {
        file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'worker.error.log', "Code: $exit_code, Signal: $signal\n\n");
    }
}
