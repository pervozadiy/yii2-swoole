<?php

declare(strict_types=1);

namespace app\modules\swoole\server;

use yii\base\Model;

class ServerConfig extends Model
{
    public $reactor_num;
    public $worker_num;
    public $max_request;
    public $max_conn;
    public $task_worker_num;
    public $task_ipc_mode;
    public $task_max_request;
    public $task_tmpdir;
    public $dispatch_mode;
    public $dispatch_func;
    public $message_queue_key;
    public $daemonize;
    public $backlog;
    public $log_file;
    public $log_level;
    public $heartbeat_check_interval;
    public $heartbeat_idle_time;
    public $open_eof_check;
    public $open_eof_split;
    public $package_eof;
    public $open_length_check;
    public $package_length_type;
    public $package_length_func;
    public $package_max_length;
    public $open_cpu_affinity;
    public $cpu_affinity_ignore;
    public $open_tcp_nodelay;
    public $tcp_defer_accept;
    public $ssl_cert_file;
    public $ssl_method;
    public $ssl_ciphers;
    public $user;
    public $group;
    public $chroot;
    public $pid_file;
    public $pipe_buffer_size;
    public $buffer_output_size;
    public $socket_buffer_size;
    public $enable_unsafe_event;
    public $discard_timeout_request;
    public $enable_reuse_port;
    public $enable_delay_receive;
    public $open_http_protocol;
    public $open_http2_protocol;
    public $open_websocket_protocol;
    public $open_mqtt_protocol;
    public $upload_tmp_dir;
    public $http_parse_post;
    public $document_root;
    public $enable_static_handler;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        //TODO add validation rules for config
        return [
            [$this->attributes(), 'safe']
        ];
    }

    public function formName()
    {
        return 'settings';
    }

    public function __construct(array $rawConfig, array $config = [])
    {
        parent::__construct($config);

        $this->load($rawConfig);
    }

    /**
     * @return array
     */
    public function getConfigArray(): array
    {
        $cpuCores = \swoole_cpu_num();
        if ($this->reactor_num === null) {
            $this->reactor_num = $cpuCores;
        }

        if ($this->worker_num === null) {
            $this->worker_num = 2 * $cpuCores;
        }

        if ($this->document_root !== null) {
            $this->enable_static_handler = true;
        }

        return $this->attributes;
    }

    /**
     * @throws \RuntimeException
     * @return int
     */
    public function getPid(): int
    {
        /** @var string $contents */
        $contents = file_get_contents($this->getPidFile());


        return (int)$contents;
    }

    /**
     * @throws \RuntimeException
     * @return string
     */
    public function getPidFile(): string
    {
        if (!file_exists($this->pid_file)) {
            throw new \RuntimeException('Pid file does not exist');
        }

        return $this->pid_file;
    }
}
