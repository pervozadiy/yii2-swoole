<?php

namespace app\modules\swoole\server\events;

use Swoole\Http\Server;
use yii\base\Event;

class WorkerStopEvent extends ServerEvent
{
    /**
     * @var Server
     */
    public $server;

    /**
     * @var int
     */
    public $workerId;

    public function mapArgsToProps(array $args): void
    {
        $this->server = $args[0];
        $this->workerId = $args[1];
    }
}
