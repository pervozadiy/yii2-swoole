<?php

namespace app\modules\swoole\server\events;

use Swoole\Http\Request as SwooleRequest;
use Swoole\Http\Response;
use yii\base\Event;

class RequestEvent extends ServerEvent
{
    /**
     * @var SwooleRequest
     */
    public $request;

    /**
     * @var Response
     */
    public $response;

    public function mapArgsToProps(array $args): void
    {
        $this->request = $args[0];
        $this->response = $args[1];
    }
}
