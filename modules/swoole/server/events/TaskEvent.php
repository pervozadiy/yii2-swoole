<?php

namespace app\modules\swoole\server\events;

use Swoole\Http\Server;
use yii\base\Event;

class TaskEvent extends ServerEvent
{
    /**
     * @var Server
     */
    public $server;

    /**
     * @var int
     */
    public $taskId;

    /**
     * @var int
     */
    public $srcWorkerId;

    public function mapArgsToProps(array $args): void
    {
        $this->server = $args[0];
        $this->taskId = $args[1];
        $this->srcWorkerId = $args[2];
    }
}
