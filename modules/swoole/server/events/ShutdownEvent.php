<?php

namespace app\modules\swoole\server\events;

use Swoole\Http\Server;
use yii\base\Event;

class ShutdownEvent extends ServerEvent
{
    /**
     * @var Server
     */
    public $server;

    public function mapArgsToProps(array $args): void
    {
        $this->server = $args[0];
    }
}
