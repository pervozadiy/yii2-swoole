<?php

namespace app\modules\swoole\server\events;

use yii\base\Event;

class ServerEvent extends Event
{
    /**
     * Maps event handler arguments to Event's properties
     * @param array $args
     */
    public function mapArgsToProps(array $args): void
    {
        $this->data = $args;
    }
}
