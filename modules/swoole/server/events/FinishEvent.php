<?php

namespace app\modules\swoole\server\events;

use Swoole\Http\Server;
use yii\base\Event;

class FinishEvent extends ServerEvent
{
    /**
     * @var Server
     */
    public $server;

    /**
     * @var int
     */
    public $taskId;

    /**
     * @var string
     */
    public $data;

    public function mapArgsToProps(array $args): void
    {
        $this->server = $args[0];
        $this->taskId = $args[1];
        $this->data = $args[2];
    }
}
