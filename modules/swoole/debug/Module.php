<?php

namespace app\modules\swoole\debug;

use app\modules\swoole\web\NullApp;

class Module extends \yii\debug\Module
{
    public function init()
    {
        parent::init();
        if (\Yii::$app instanceof NullApp) {
            $this->setViewPath('@vendor/yiisoft/yii2-debug/src/views');
            $this->initPanels();
        }
    }
}
