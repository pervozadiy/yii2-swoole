<?php

declare(strict_types=1);

namespace app\modules\swoole\bootstrap;

class BootstrapManager
{
    /**
     * @var iterable
     */
    private $bootstrappers;

    public function __construct(Bootstrapper ...$bootstrappers)
    {
        $this->bootstrappers = $bootstrappers;
    }

    /**
     * @param array $runtimeConfig
     */
    public function boot(array $runtimeConfig = []): void
    {
        foreach ($this->bootstrappers as $bootstrapper) {
            $bootstrapper->attach();
        }
    }
}
