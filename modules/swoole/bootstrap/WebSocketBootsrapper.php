<?php

namespace app\modules\swoole\bootstrap;

use app\modules\swoole\server\events\WorkerStartEvent;
use app\modules\swoole\server\Manager;
use app\modules\swoole\websocket\Parser;
use app\modules\swoole\websocket\RoomStorage;
use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\SocketIoParser;
use app\modules\swoole\websocket\socketIo\Transport;
use app\modules\swoole\websocket\socketIo\transports\Polling;
use app\modules\swoole\websocket\socketIo\transports\Websocket;
use Yii;
use yii\base\BaseObject;
use function in_array;

class WebSocketBootsrapper extends BaseObject implements Bootstrapper
{
    /**
     * @var Manager
     */
    private $server;

    /**
     * @var Broadcast
     */
    private $broadcast;

    /**
     * @var Transport[]
     */
    private $_transports = [];

    /**
     * @var Parser
     */
    private $parser;

    public $parserClass = SocketIoParser::class;

    /**
     * @var array
     */
    public $IOConfig = [];

    /**
     * @var array
     */
    public $transports = ['polling', 'websocket'];

    public function __construct(Manager $server, $config = [])
    {
        parent::__construct($config);

        $this->server = $server;
        $room = new RoomStorage([
            'room_rows' => 4096,
            'room_size' => 2048,
            'client_rows' => 8192,
            'client_size' => 2048,
        ]);
        $room->prepare();
        $this->parser = Yii::createObject($this->parserClass);
        $this->broadcast = new Broadcast();
    }

    public function attach(): void
    {
        if (in_array('polling', $this->transports, true)) {
            $this->_transports[] = Yii::createObject(array_merge($this->IOConfig, [
                'class' => Polling::class
            ]), [$this->broadcast]);
            $this->createHandler('request');
        }
        if (in_array('websocket', $this->transports, true)) {
            $this->_transports[] = Yii::createObject(array_merge($this->IOConfig, [
                'class' => Websocket::class
            ]), [$this->broadcast]);
            $this->createHandler('message');
            $this->createHandler('open');
            $this->createHandler('close');
        }

        $this->server->on(Manager::EVENT_WORKER_START, [$this, 'registerDependencies']);
    }

    private function createHandler(string $event)
    {
        $method = 'on' . ucfirst($event);
        $this->server->server->on($event, function (...$args) use ($event, $method) {
            foreach ($this->_transports as $transport) {
                if ($transport->isApplicable($event) && $transport->$method(...$args)) {
                    return;
                }
            }
            // trigger default listener
            $this->server->getEventListener($event)(...$args);
        });
    }

    public function registerDependencies(WorkerStartEvent $event)
    {
        if (!$event->server->taskworker) {
            Yii::$container->setPersistentObj(Broadcast::class, $this->broadcast);
            Yii::$container->setPersistentObj(Parser::class, $this->parser);
        } else {
            Yii::$container->setSingleton(Broadcast::class, function () {
                return $this->broadcast;
            });
            Yii::$container->setSingleton(Parser::class, function () {
                $this->parser;
            });
        }
    }
}
