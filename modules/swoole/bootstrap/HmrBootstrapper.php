<?php

declare(strict_types=1);

namespace app\modules\swoole\bootstrap;

use app\modules\swoole\server\events\WorkerStartEvent;
use app\modules\swoole\server\Manager;
use Swoole\Http\Server;
use yii\base\BaseObject;

class HmrBootstrapper extends BaseObject implements Bootstrapper
{
    /**
     * @var int
     */
    public $interval = 500;
    /**
     * @var Manager
     */
    private $server;

    /**
     * @var resource
     */
    private $inotify;

    /**
     * @var array
     */
    private $watchedFiles = [];

    /**
     * @var array
     */
    private $nonReloadableFiles = [];

    public function __construct(Manager $server, array $config = [])
    {
        $this->server = $server;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function attach(): void
    {
        $this->setNonReloadableFiles(\get_included_files());
        $this->initInotify();

        $this->server->on(Manager::EVENT_WORKER_START, [$this, 'initWatcher']);
    }

    private function initInotify(): void
    {
        $this->inotify = inotify_init();
        \stream_set_blocking($this->inotify, false);
    }

    public function initWatcher(WorkerStartEvent $event): void
    {
        if ($event->workerId === 0) {
            $event->server->tick($this->interval, function () use ($event) {
                $this->tick($event->server);
            });
        }
    }

    private function tick(Server $server): void
    {
        $events = inotify_read($this->inotify);
        if (false !== $events) {
            $this->server->reload();
        }
        $this->watchFiles(\get_included_files());
    }

    /**
     * @param array $files
     */
    private function watchFiles(array $files): void
    {
        foreach ($files as $file) {
            if (!isset($this->nonReloadableFiles[$file]) && !isset($this->watchedFiles[$file])) {
                $this->watchedFiles[$file] = \inotify_add_watch($this->inotify, $file, IN_MODIFY);
            }
        }
    }

    /**
     * @param string[] $nonReloadableFiles
     */
    private function setNonReloadableFiles(array $nonReloadableFiles): void
    {
        foreach ($nonReloadableFiles as $nonReloadableFile) {
            if (file_exists($nonReloadableFile)) {
                $this->nonReloadableFiles[$nonReloadableFile] = true;
            }
        }
    }
}
