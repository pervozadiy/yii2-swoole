<?php

namespace app\modules\swoole\bootstrap;

use app\modules\swoole\server\events\WorkerStartEvent;
use app\modules\swoole\server\Manager;
use app\modules\swoole\task\SwooleQueue;
use yii\base\BaseObject;

class TaskBootstrapper extends BaseObject implements Bootstrapper
{
    /**
     * @var Manager
     */
    private $server;

    private $queue;

    public function __construct(Manager $server, $config = [])
    {
        $this->server = $server;
        $this->queue = new SwooleQueue($this->server);

        parent::__construct($config);
    }

    public function attach(): void
    {
        $this->server->server->on(Manager::EVENT_TASK, [$this->queue, 'processTask']);
        $this->server->server->on(Manager::EVENT_FINISH, [$this->queue, 'taskFinish']);
        $this->server->on(Manager::EVENT_WORKER_START, [$this, 'onStart']);
    }

    public function onStart(WorkerStartEvent $event)
    {
        if (!$event->server->taskworker) {
            \Yii::$container->setPersistentObj(SwooleQueue::class, $this->queue);
        }
    }
}
