<?php

namespace app\modules\swoole\bootstrap;

interface Bootstrapper
{
    public function attach(): void;

//    public function setConfig(array $config): Bootstrapper;
}