<?php

declare(strict_types=1);

namespace app\modules\swoole\bootstrap;

use app\modules\swoole\coroutine\Context;
use app\modules\swoole\server\events\RequestEvent;
use app\modules\swoole\server\events\StartEvent;
use app\modules\swoole\server\events\WorkerStartEvent;
use app\modules\swoole\server\events\WorkerStopEvent;
use app\modules\swoole\server\Manager;
use app\modules\swoole\web\Application;
use Yii;
use yii\base\BaseObject;

class WebBootstrapper extends BaseObject implements Bootstrapper
{
    public $index = '/index.php';

    public $webRoot;

    /**
     * @var array;
     */
    public $appConfig;

    /**
     * @var Manager
     */
    private $server;

    public function __construct(Manager $server, $config = [])
    {
        $this->server = $server;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function attach(): void
    {
        $this->server->on(Manager::EVENT_START, [$this, 'onStart']);
        $this->server->on(Manager::EVENT_WORKER_START, [$this, 'onWorkerStart']);
        $this->server->on(Manager::EVENT_REQUEST, [$this, 'onRequest']);
    }

    public function onStart(StartEvent $event)
    {
        $this->webRoot = $this->webRoot ?? $event->server->setting['document_root'] ?? '/web';
    }

    /**
     * @param WorkerStartEvent $event
     */
    public function onWorkerStart(WorkerStartEvent $event)
    {
        $_SERVER['PHP_SELF'] = $_SERVER['SCRIPT_NAME'] = $this->index;
        $_SERVER['SCRIPT_FILENAME'] = $this->webRoot . $this->index;
    }

    /**
     * @param RequestEvent $event
     */
    public function onRequest(RequestEvent $event)
    {
        try {
            new Application($this->appConfig);
            Yii::$app->setAliases($this->buildAliases());
            Yii::$app->getRequest()->setSwooleRequest($event->request);
            Yii::$app->getResponse()->setSwooleResponse($event->response);
            Yii::$app->run();
        } catch (\Throwable $throwable) {
            var_dump($throwable->getMessage(), $throwable->getTraceAsString());
            Yii::error($throwable);
        } finally {
            $this->onEndRequest();
        }
    }

    private function buildAliases(): array
    {
        $aliases = [
            '@web' => '',
            '@webroot' => $this->webRoot,
        ];

        return isset($this->appConfig['aliases']) ? array_merge($aliases, $this->appConfig['aliases']) : $aliases;
    }

    /**
     * @param WorkerStopEvent $event
     */
    public function onWorkerStop(WorkerStopEvent $event)
    {
        $contexts = Yii::$context->getCoreInstances();
        foreach ($contexts as $context) {
            /** @var Application $application */
            $application = $context[Context::COROUTINE_APP] ?? null;
            if ($application === null) {
                continue;
            }
            $targets = $application->getLog()->targets;
            foreach ($targets as $target) {
                $target->export();
            }
        }
    }

    /**
     * To flush log
     * To destroy context
     */
    private function onEndRequest()
    {
        Yii::getLogger()->flush();
        Yii::getLogger()->flush(true);
        Yii::$app->session->persist();
//        Yii::$app->db->close();
//        Yii::$context->destroy();
        var_dump('REQUEST ENDED!');
    }
}
