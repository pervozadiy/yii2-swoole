<?php

declare(strict_types=1);

namespace app\modules\swoole\web;

use app\modules\swoole\server\Manager;
use Swoole\Coroutine;
use Yii;
use function call_user_func_array;

class NullApp
{
    private $_server;

    private $prev;

    public function __construct(Manager $server)
    {
        $this->_server = $server;
        $this->prev = Yii::$app;
    }

    public function getServer(): Manager
    {
        return $this->_server;
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            // read property, e.g. getName()
            return $this->$getter();
        }
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        $application = $this->getApplication();

        if (property_exists($application, $name)) {
            return $application->{$name};
        }
        if (method_exists($application, $getter)) {
            // read property, e.g. getName()
            return $application->$getter();
        }
        return $application->{$name};

    }

    public function __set($name, $value)
    {
        $application = $this->getApplication();
        $application->{$name} = $value;
    }

    public function __call($name, $arguments)
    {
        $application = $this->getApplication();
        return call_user_func_array([$application, $name], $arguments);
    }

    /**
     * @return \yii\base\Application
     */
    public function getApplication(): \yii\base\Application
    {
        if (Yii::$context->getCid() <= 1) {
            return $this->prev;
        }
        if (null === $application = Yii::$context->getApplication()) {
            throw new \RuntimeException('Current coroutine context has no Application instance! ' . Coroutine::getCid());
        }

        return $application;
    }
}
