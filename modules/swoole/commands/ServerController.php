<?php

declare(strict_types=1);

namespace app\modules\swoole\commands;

use app\modules\swoole\bootstrap\BootstrapManager;
use app\modules\swoole\configurator\Configurator;
use app\modules\swoole\Module;
use app\modules\swoole\server\events\StartEvent;
use app\modules\swoole\server\Factory;
use app\modules\swoole\server\Manager;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Class ServerController
 * @package app\modules\swoole\commands
 */
class ServerController extends Controller
{
    /**
     * @var BootstrapManager
     */
    private $bootstrapper;

    /**
     * @var Configurator
     */
    private $configurator;

    /**
     * @var Manager
     */
    private $server;

    public function __construct(
        string $id,
        Module $module,
        BootstrapManager $bootstrapper,
        Configurator $configurator,
        Manager $server,
        array $config = []
    )
    {
        parent::__construct($id, $module, $config);
        $this->bootstrapper = $bootstrapper;
        $this->configurator = $configurator;
        $this->server = $server;
    }

    public function init(): void
    {
        parent::init();
    }

    public function actionStart(): int
    {
        if ($this->server->isRunning()) {
            $this->stderr('Server is already running' . PHP_EOL, Console::FG_RED);
            return ExitCode::USAGE;
        }
        $this->server->on(Manager::EVENT_START, function (StartEvent $event) {
            $this->stdout("Server listening on http://{$event->server->host}:{$event->server->port}" . PHP_EOL, Console::FG_GREEN);
        });

        try {
            $this->prepareServer();
        } catch (\Exception $exception) {
            $this->stderr($exception->getMessage() . PHP_EOL, Console::FG_RED);
            return ExitCode::CONFIG;
        }

        if ($this->server->start()) {
            return ExitCode::OK;
        }
        $this->stderr(ExitCode::getReason(ExitCode::UNSPECIFIED_ERROR) . PHP_EOL, Console::FG_RED);

        return ExitCode::UNSPECIFIED_ERROR;
    }

    /**
     * @throws InvalidConfigException
     */
    private function prepareServer(): void
    {
        $server = \Yii::$container->invoke([Factory::class, 'create']);
        $this->configurator->configure($server);
        $this->server->attach($server);
        $this->bootstrapper->boot();
    }

    public function actionStop(): int
    {
        try {
            if ($this->server->shutdown()) {
                $this->stdout('Server successfully stopped!' . PHP_EOL, Console::FG_GREEN);
                return ExitCode::OK;
            }
            $this->stdout('Failed to stop server!' . PHP_EOL, Console::FG_RED);
            return ExitCode::UNSPECIFIED_ERROR;
        } catch (\RuntimeException $e) {
            $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
            \Yii::error($e, __METHOD__);

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    public function actionReload(): int
    {
        try {
            if ($this->server->reload()) {
                $this->stdout('Server successfully reloaded!' . PHP_EOL, Console::FG_GREEN);
                return ExitCode::OK;
            }
            $this->stdout('Failed to reload server!' . PHP_EOL, Console::FG_RED);
            return ExitCode::UNSPECIFIED_ERROR;
        } catch (\RuntimeException $e) {
            $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
            \Yii::error($e, __METHOD__);

            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
