<?php

declare(strict_types=1);

namespace app\modules\swoole\coroutine;

use app\modules\swoole\di\Container;
use Swoole\Coroutine;
use yii\base\Application;

class Context
{
    public const COROUTINE_CONTAINER = 'container';

    public const COROUTINE_APP = 'app';

    /**
     * @var Context
     */
    private static $instance;

    public static function getInstance(): Context
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    /**
     * The data in different coroutine environment.
     *
     * @var array
     */
    protected $coroutineData = [];

    /**
     * Yii::$app and Yii::$container instances in different coroutine environment.
     *
     * @var array
     */
    private $coreInstances = [];

    /**
     * @var int[]
     */
    private $stackTree = [];


    /**
     * @param string $key
     * @param $val
     * @return Context
     */
    public function setContextData(string $key, $val): Context
    {
        $tid = $this->getTid();
        $this->coroutineData[$tid][$key] = $val;

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function getContextData(string $key, $default = null)
    {
        $tid = $this->getTid();

        return $this->coroutineData[$tid][$key] ?? $default;
    }

    /**
     * @return array
     */
    public function getCoreInstances(): array
    {
        return $this->coreInstances;
    }

    /**
     * @param Application $application
     * @return Context
     */
    public function setApplication(Application $application): Context
    {
        return $this->setCoreInstance(static::COROUTINE_APP, $application);
    }

    /**
     * @return Application
     */
    public function getApplication(): ?Application
    {
        return $this->findCoreInstance(static::COROUTINE_APP);
    }

    public function setContainer(Container $container): Context
    {
        return $this->setCoreInstance(static::COROUTINE_CONTAINER, $container);
    }

    /**
     * @return Container
     */
    public function getContainer(): ?Container
    {
        return $this->findCoreInstance(static::COROUTINE_CONTAINER);
    }

    /**
     * @param string $key
     * @param $instance
     * @return Context
     */
    protected function setCoreInstance(string $key, $instance): Context
    {
        $coroutineId = $this->getTid();
        $this->coreInstances[$coroutineId][$key] = $instance;

        return $this;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    protected function findCoreInstance($key)
    {
        $coroutineId = $this->getTid();

        return $this->coreInstances[$coroutineId][$key] ?? null;
    }

    public function destroy(): void
    {
        $tid = $this->getTid();
        if (isset($this->coroutineData[$tid])) {
            unset($this->coroutineData[$tid]);
        }
        if (isset($this->coreInstances[$tid])) {
            unset($this->coreInstances[$tid]);
        }
        foreach ($this->stackTree as $key => $value) {
            if ($tid === $value) {
                unset($this->stackTree[$key]);
            }
        }
    }

    /**
     * Gets top coroutine ID
     * @return int
     */
    public function getTid(): int
    {
        $id = $this->getCid();

        return $this->stackTree[$id] ?? $id;
    }

    /**
     * Gets current coroutine ID
     * @return int
     */
    public function getCid(): int
    {
        return Coroutine::getCid();
    }

    /**
     * Create child coroutine in case to use parent`s context
     *
     * @param callable $callback
     * @param mixed ...$args
     * @return int
     */
    public function createChild(callable $callback, ...$args): int
    {
        $tid = $this->getTid();

        return go(function () use ($callback, $args, $tid) {
            $this->bindToParent($tid);
            try {
                $callback(...$args);
            } catch (\Throwable $exception) {
                \Yii::error($exception, "Coroutine $tid internal exception");
            }
        });
    }

    /**
     * @param $parentCid
     * @return bool
     */
    public function bindToParent($parentCid): bool
    {
        $id = $this->getCid();
        if ($id === $parentCid) {
            return false;
        }
        if ($this->stackTree[$parentCid]) {
            $parentCid = $this->stackTree[$parentCid];
        }
        $this->stackTree[$id] = $parentCid;
        return true;
    }
}
