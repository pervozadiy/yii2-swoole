<?php

declare(strict_types=1);

namespace app\modules\swoole;

use app\modules\swoole\bootstrap\BootstrapManager;
use app\modules\swoole\bootstrap\HmrBootstrapper;
use app\modules\swoole\bootstrap\WebBootstrapper;
use app\modules\swoole\server\Manager;
use app\modules\swoole\server\ServerConfig;
use app\modules\swoole\server\SocketConfig;
use app\modules\swoole\server\WebsocketConfig;
use Yii;

class Module extends \yii\base\Module
{
    /**
     * @var array
     */
    public $config;

    public $bootstrappers = [
        WebBootstrapper::class,
    ];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        Yii::$container->setSingleton(ServerConfig::class, [], [$this->config]);
        Yii::$container->setSingleton(SocketConfig::class, [], [$this->config]);
        Yii::$container->setSingleton(WebsocketConfig::class, [], [$this->config]);
        Yii::$container->setSingleton(Manager::class);
//        Yii::$container->setSingleton(ServerProxy::class, HttpServer::class);
//        Yii::$container->setSingletons($diConfig['singletons']);
//        Yii::$container->setDefinitions($diConfig['definitions']);
        $this->buildBootstrapManager();

        $this->controllerNamespace = __NAMESPACE__ . '\commands';

        parent::init();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    private function buildBootstrapManager(): void
    {
        $instances = [];
        foreach ($this->bootstrappers as $bootstrapper) {
            $instances[] = Yii::createObject($bootstrapper);
        }
        Yii::$container->setSingleton(BootstrapManager::class, [], $instances);
    }
}