<?php

namespace app\modules\swoole\components;

use app\modules\swoole\web\NullApp;
use yii\debug\Module;
use Yii;

class DebugModule extends Module
{
    public function init()
    {
        \yii\base\Module::init();
        $this->dataPath = Yii::getAlias($this->dataPath);

        if (Yii::$app instanceof NullApp) {
            try {
                $this->initPanels();
            } catch (\Throwable $e) {
                var_dump($e->getMessage(), $e->getTraceAsString());
            }

        }
    }
}
