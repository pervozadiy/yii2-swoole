<?php

namespace app\modules\swoole\storage;

use Swoole\Coroutine\Channel;

class SwooleChannelStorage
{
    /**
     * Registered swoole tables.
     *
     * @var Channel[]
     */
    private $_channels = [];

    /**
     * @param string $name
     * @param Channel $channel
     *
     * @return SwooleChannelStorage
     */
    public function add(string $name, Channel $channel): self
    {
        $this->_channels[$name] = $channel;

        return $this;
    }

    /**
     * Get a swoole table by its name from existing tables.
     *
     * @param string $name
     *
     * @return Channel|null $table
     */
    public function get(string $name): ?Channel
    {
        return $this->_channels[$name] ?? null;
    }

    public function has(string $name): bool
    {
        return isset($this->_channels[$name]);
    }

    /**
     * Get all existing swoole tables.
     *
     * @return Channel[]
     */
    public function getAll(): array
    {
        return $this->_channels;
    }

    /**
     * Dynamically access table.
     *
     * @param  string $key
     *
     * @return Channel|null
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    public function __set(string $name, Channel $value)
    {
        $this->add($name, $value);
    }

    public function __isset(string $name)
    {
        return $this->has($name);
    }
}
