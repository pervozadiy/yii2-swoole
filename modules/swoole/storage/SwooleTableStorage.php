<?php

namespace app\modules\swoole\storage;

use Swoole\Table;

class SwooleTableStorage
{
    /**
     * Registered swoole tables.
     *
     * @var array
     */
    private $_tables = [];

    /**
     * Add a swoole table to existing tables.
     *
     * @param string $name
     * @param \Swoole\Table $table
     *
     * @return SwooleTableStorage
     */
    public function add(string $name, Table $table): self
    {
        $this->_tables[$name] = $table;

        return $this;
    }

    /**
     * Get a swoole table by its name from existing tables.
     *
     * @param string $name
     *
     * @return Table|null $table
     */
    public function get(string $name): ?Table
    {
        return $this->_tables[$name] ?? null;
    }

    public function has(string $name): bool
    {
        return isset($this->_tables[$name]);
    }

    /**
     * Get all existing swoole tables.
     *
     * @return Table[]
     */
    public function getAll(): array
    {
        return $this->_tables;
    }

    /**
     * Dynamically access table.
     *
     * @param  string $key
     *
     * @return Table|null
     */
    public function __get(string $key)
    {
        return $this->get($key);
    }

    public function __set(string $name, Table $value)
    {
        $this->add($name, $value);
    }

    public function __isset(string $name)
    {
        return $this->has($name);
    }
}
