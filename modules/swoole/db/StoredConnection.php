<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

class StoredConnection extends \yii\db\Connection
{
    public const TYPE_MASTER = 'master_connection';

    public const TYPE_SLAVE = 'slave_connection';

    public $commandClass = Command::class;

    public $pdoClass = PDO::class;

    public $__lat;

    public $type;

    public $poolId;

    /**
     * @var ConnectionManager
     */
    private $cm;

    private $_master = false;

    private $_slave = false;

    private $_transaction;

    private $_schema;

    public function __construct(ConnectionManager $cm, $config = [])
    {
        $this->cm = $cm;
        parent::__construct($config);
    }

    public function init()
    {
        $this->on(self::EVENT_COMMIT_TRANSACTION, function () {
            $this->cm->trigger(self::EVENT_COMMIT_TRANSACTION);
        });
        $this->on(self::EVENT_ROLLBACK_TRANSACTION, function () {
            $this->cm->trigger(self::EVENT_ROLLBACK_TRANSACTION);
        });
    }

    /**
     * @param bool $fallbackToMaster
     * @return StoredConnection
     * @throws \Smf\ConnectionPool\BorrowConnectionTimeoutException
     */
    public function getSlave($fallbackToMaster = true): ?StoredConnection
    {
        if ($this->_slave === false) {
            $this->_slave = $this->cm->getSlave(false);
        }
        return $this->_slave === null && $fallbackToMaster ? $this : $this->_slave;
    }

    public function getSlavePdo($fallbackToMaster = true)
    {
        $db = $this->getSlave(false);
        if ($db === null) {
            return $fallbackToMaster ? $this->getMasterPdo() : null;
        }
        $db->open();

        return $db->pdo;
    }

    public function reset(): void
    {
        $this->_master = false;
        $this->_slave = false;
        $this->_schema = null;
        if ($this->transaction->isActive) {
            $this->transaction->rollBack();
        }
        $this->_transaction = null;
    }

    public function release(): void
    {
        if ($this->_slave) {
            $this->cm->releaseSlave($this->_slave);
        }
        $this->cm->releaseMaster($this);
    }
}
