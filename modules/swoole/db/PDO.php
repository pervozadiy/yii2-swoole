<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Swoft\Stdlib\Helper\ArrayHelper;
use Swoole\Coroutine\MySQL;
use yii\db\Exception;
use function count;
use function in_array;

class PDO extends \PDO
{
    public static $keyMap = [
        'dbname' => 'database',
    ];

    /**
     * @var MySQL
     */
    public $client;

    /**
     * @var MySQL\Statement
     */
    private $lastStmt;

    private static $options = [
        'host' => '',
        'port' => 3306,
        'user' => '',
        'password' => '',
        'database' => '',
        'charset' => 'utf8mb4',
        'strict_type' => true,
        'timeout' => -1,
        'fetch_mode' => true
    ];

    private $inTransaction = false;

    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $options
     *
     * @throws Exception
     */
    public function __construct(string $dsn, string $username = '', string $password = '', ?array $options = [])
    {
        $options = $options ?? [];
        parent::__construct($dsn, $username, $password, $options);
        $this->setClient();
        $this->connect($this->getOptions($dsn, $username, $password, $options));
    }

    /**
     * @param mixed $client
     */
    protected function setClient($client = null)
    {
        $this->client = $client ?: new MySQL;
    }

    /**
     * @param array $options
     *
     * @return $this
     * @throws Exception
     */
    protected function connect(array $options = []): self
    {
        $this->client->connect($options);

        if (!$this->client->connected) {
            $message = $this->client->connect_error ?: $this->client->error;
            $errorCode = $this->client->connect_errno ?: $this->client->errno;

            throw new Exception($message, $errorCode);
        }

        return $this;
    }

    /**
     * @param string $dsn
     * @param string $username
     * @param string $password
     * @param array $driverOptions
     *
     * @return array
     */
    protected function getOptions(string $dsn, string $username, string $password, array $driverOptions): array
    {
        $dsn = explode(':', $dsn);
        $driver = ucwords(array_shift($dsn));
        $dsn = explode(';', implode(':', $dsn));
        $configuredOptions = [];

        static::checkDriver($driver);

        foreach ($dsn as $kv) {
            $kv = explode('=', $kv);
            if (count($kv)) {
                $configuredOptions[$kv[0]] = $kv[1] ?? '';
            }
        }

        $authorization = [
            'user' => $username,
            'password' => $password,
        ];

        $configuredOptions = $driverOptions + $authorization + $configuredOptions;

        foreach (static::$keyMap as $pdoKey => $swpdoKey) {
            if (isset($configuredOptions[$pdoKey])) {
                $configuredOptions[$swpdoKey] = $configuredOptions[$pdoKey];
                unset($configuredOptions[$pdoKey]);
            }
        }

        return array_merge(static::$options, $configuredOptions);
    }

    /**
     * @param string $driver
     */
    public static function checkDriver(string $driver): void
    {
        if (!in_array($driver, static::getAvailableDrivers(), true)) {
            throw new \InvalidArgumentException("{$driver} driver is not supported yet.");
        }
    }

    /**
     * @return array
     */
    public static function getAvailableDrivers(): array
    {
        return ['Mysql'];
    }

    /**
     * @return bool|void
     */
    public function beginTransaction()
    {
        $this->client->begin();
        $this->inTransaction = true;
    }

    /**
     * @return bool|void
     */
    public function rollBack()
    {
        $this->client->rollback();
        $this->inTransaction = false;
    }

    /**
     * @return bool|void
     */
    public function commit()
    {
        $this->client->commit();
        $this->inTransaction = true;
    }

    /**
     * @return bool
     */
    public function inTransaction(): bool
    {
        return $this->inTransaction;
    }

    /**
     * @param null $seqname
     *
     * @return int|string
     */
    public function lastInsertId($seqname = null)
    {
        return $this->client->insert_id;
    }

    /**
     * @return mixed|void
     */
    public function errorCode()
    {
        $this->client->errno;
    }

    /**
     * @return array
     */
    public function errorInfo()
    {
        return [
            $this->client->errno,
            $this->client->errno,
            $this->client->error,
        ];
    }


    /**
     * @param string $statement
     * @return int
     * @throws Exception
     */
    public function exec($statement): int
    {
        $this->query($statement);

        return $this->client->affected_rows;
    }

    /**
     * @param string $statement
     * @param int $mode
     * @param null $arg3
     * @param array $ctorargs
     * @return array|\PDOStatement
     * @throws Exception
     */
    public function query($statement, $mode = PDO::ATTR_DEFAULT_FETCH_MODE, $arg3 = null, array $ctorargs = [])
    {
        $result = $this->client->query($statement, ArrayHelper::get(self::$options, 'timeout'));

        if ($result === false) {
            throw new Exception($this->client->error, [], $this->client->errno);
        }

        return $result;
    }

    /**
     * @param string $statement
     * @param array $options
     * @return PDOStatement
     * @throws Exception
     */
    public function prepare($statement, $options = []): PDOStatement
    {
        $bindKeyMap = [];
        if (strpos($statement, ':') !== false) {
            $i = 0;
            $statement = preg_replace_callback('/:([a-zA-Z_]\w*?)\b/', function ($matches) use (&$i, &$bindKeyMap) {
                $bindKeyMap[$matches[1]] = $i++;

                return '?';
            }, $statement);
        }
        if ($this->lastStmt) {
            $this->lastStmt->nextResult();
        }

        $stmtObj = $this->lastStmt = $this->client->prepare($statement);
        if ($stmtObj) {
            return new PDOStatement($stmtObj, $bindKeyMap);
        }

        throw new Exception($this->client->error, [], $this->client->errno);
    }

    /**
     * @param int $attribute
     *
     * @return bool|mixed|string
     */
    public function getAttribute($attribute)
    {
        switch ($attribute) {
            case \PDO::ATTR_AUTOCOMMIT:
                return true;
            case \PDO::ATTR_CASE:
            case \PDO::ATTR_CLIENT_VERSION:
            case \PDO::ATTR_CONNECTION_STATUS:
                return $this->client->connected;
            case \PDO::ATTR_DRIVER_NAME:
            case \PDO::ATTR_ERRMODE:
                return 'Swoole Style';
            case \PDO::ATTR_ORACLE_NULLS:
            case \PDO::ATTR_PERSISTENT:
            case \PDO::ATTR_PREFETCH:
            case \PDO::ATTR_SERVER_INFO:
                return self::$options['timeout'];
            case \PDO::ATTR_SERVER_VERSION:
                return 'Swoole Mysql';
            case \PDO::ATTR_TIMEOUT:
            default:
                throw new \InvalidArgumentException('Not implemented yet!');
        }
    }

    public function quote($string, $paramtype = null)
    {
        return $this->client->escape($string);
    }

    public function close()
    {
        $this->client->close();
    }

    public function __destruct()
    {
        $this->close();
    }
}
