<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Smf\ConnectionPool\Connectors\ConnectorInterface;

class PoolConnector implements ConnectorInterface
{
    private $cm;

    public function __construct(ConnectionManager $cm)
    {
        $this->cm = $cm;
    }

    /**
     * Connect to the specified Server and returns the connection resource
     * @param array $config
     * @return StoredConnection
     * @throws \yii\base\InvalidConfigException
     */
    public function connect(array $config): StoredConnection
    {
        return \Yii::createObject(array_merge(['class' => StoredConnection::class], $config), [$this->cm]);
    }

    /**
     * Disconnect and free resources
     * @param StoredConnection $connection
     */
    public function disconnect($connection)
    {
        $connection->close();
    }

    /**
     * Whether the connection is established
     * @param StoredConnection $connection
     * @return bool
     */
    public function isConnected($connection): bool
    {
        return $connection->pdo !== null;
    }

    /**
     * Reset the connection
     * @param StoredConnection $connection
     * @param array $config
     * @return StoredConnection
     */
    public function reset($connection, array $config): StoredConnection
    {
        $connection->reset();

        return $connection;
    }

    /**
     * Validate the connection
     *
     * @param StoredConnection $connection
     * @return bool
     */
    public function validate($connection): bool
    {
        return $connection instanceof \yii\db\Connection;
    }
}
