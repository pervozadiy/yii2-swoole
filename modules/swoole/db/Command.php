<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Swoole\Coroutine;
use Yii;
use yii\db\DataReader;
use yii\db\Exception;

/**
 * @package app\modules\swoole\db
 *
 * @property StoredConnection $db
 */
class Command extends \yii\db\Command
{
    private $_queried = false;

    public function release(): void
    {
        if ($this->db->getTransaction() === null) {
            $this->pdoStatement = null;
            $this->db->release();
        }
    }

    public function __destruct()
    {
        $this->release();
    }

    public function prepare($forRead = null)
    {
        if ($this->pdoStatement) {
            $this->bindPendingParams();
            return;
        }

        $sql = $this->getSql();

        if ($this->db->getTransaction()) {
            // master is in a transaction. use the same connection.
            $forRead = false;
        }
        if ($forRead || ($forRead === null && $this->db->getSchema()->isReadQuery($sql))) {
            $pdo = $this->db->getSlavePdo();
        } else {
            $pdo = $this->db->getMasterPdo();
        }

        try {
            $this->pdoStatement = $pdo->prepare($sql);
            $this->bindPendingParams();
        } catch (\Exception $e) {
            $message = $e->getMessage() . "\nFailed to prepare SQL: $sql";
            $errorInfo = $e instanceof \PDOException ? $e->errorInfo : null;
            throw new Exception($message, $errorInfo, (int) $e->getCode(), $e);
        }
    }

    protected function queryInternal($method, $fetchMode = null)
    {
        [$profile, $rawSql] = $this->logQuery('yii\db\Command::query');

        if ($method !== '') {
            $info = $this->db->getQueryCacheInfo($this->queryCacheDuration, $this->queryCacheDependency);
            if (is_array($info)) {
                /* @var $cache \yii\caching\CacheInterface */
                $cache = $info[0];
                $rawSql = $rawSql ?: $this->getRawSql();
                $cacheKey = $this->getCacheKey($method, $fetchMode, $rawSql);
                $result = $cache->get($cacheKey);
                if (is_array($result) && isset($result[0])) {
                    Yii::debug('Query result served from cache', 'yii\db\Command::query');
                    return $result[0];
                }
            }
        }

        $this->prepare(true);

        try {
            $profile and Yii::beginProfile($rawSql, 'yii\db\Command::query');
            if (!$this->_queried) {
                $this->_queried = true;
                $this->internalExecute($rawSql);
            }

            if ($method === '') {
                $result = new DataReader($this);
            } else {
                if ($fetchMode === null) {
                    $fetchMode = $this->fetchMode;
                }
                $result = \call_user_func_array([$this->pdoStatement, $method], (array)$fetchMode);
//                $this->pdoStatement->closeCursor();
//                $this->_queried = false;
            }

            $profile and Yii::endProfile($rawSql, 'yii\db\Command::query');
        } catch (Exception $e) {
            $profile and Yii::endProfile($rawSql, 'yii\db\Command::query');
            throw $e;
        }

        if (isset($cache, $cacheKey, $info)) {
            $cache->set($cacheKey, [$result], $info[1], $info[2]);
            Yii::debug('Saved query result in cache', 'yii\db\Command::query');
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected function reset(): void
    {
        parent::reset();
        $this->_queried = false;
    }
}
