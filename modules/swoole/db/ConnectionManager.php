<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Smf\ConnectionPool\BorrowConnectionTimeoutException;
use Smf\ConnectionPool\ConnectionPool;
use yii;
use yii\base\InvalidConfigException;
use yii\caching\CacheInterface;
use yii\db\Connection;
use yii\db\Transaction;

class ConnectionManager extends yii\db\Connection
{
    /**
     * @var ConnectionPool
     */
    private $singleMasterPool;

    /**
     * @var PoolStorage
     */
    private $slavesStorage;

    /**
     * @var PoolStorage
     */
    private $mastersStorage;

    /**
     * @var StoredConnection
     */
    private $transactionConnection;

    public $poolSize = 10;

    public $idleNum = 2;

    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function init(): void
    {
        $this->on(self::EVENT_COMMIT_TRANSACTION, function () {
            $this->transactionConnection = null;
        });
        $this->on(self::EVENT_ROLLBACK_TRANSACTION, function () {
            $this->transactionConnection = null;
        });
    }

    /**
     * @param bool $fallbackToMaster
     * @return StoredConnection|null
     * @throws BorrowConnectionTimeoutException
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    public function getSlave($fallbackToMaster = true): ?StoredConnection
    {
        if (!$this->enableSlaves || empty($this->slaves)) {
            return $fallbackToMaster ? $this->getMaster() : null;
        }
        if ($this->slavesStorage === null) {
            $this->slavesStorage = new PoolStorage();
        }

        return $this->openFromConnectionPool($this->slavesStorage, $this->slaves, $this->slaveConfig);
//        $key = array_rand($this->slaves);
//        $config = array_merge($this->slaves[$key], $this->slaveConfig);
//        $key = 'slave_' . $key;
//        $pool = $this->slavesStorage->get($key);
//        if ($pool === null) {
//            $pool = $this->createPool($config);
//            $this->slavesStorage->add($key, $pool);
//        }
//
//        $connection = $pool->borrow();
//        $connection->poolId = $key;
//        $connection->open();
//
//        return $connection;
    }

    /**
     * @param PoolStorage $poolStorage
     * @param array $configs
     * @param array $sharedConfig
     * @return StoredConnection|null
     * @throws InvalidConfigException
     */
    protected function openFromConnectionPool(PoolStorage $poolStorage, array $configs, array $sharedConfig): ?StoredConnection
    {
        if (!isset($sharedConfig['class'])) {
            $sharedConfig['class'] = StoredConnection::class;
        }
        $cache = is_string($this->serverStatusCache) ? Yii::$app->get($this->serverStatusCache, false) : $this->serverStatusCache;
        foreach ($configs as $i => $config) {
            if (empty($config['dsn'])) {
                throw new InvalidConfigException('The "dsn" option must be specified.');
            }
            $pool = $poolStorage->get($config['dsn']);
            $key = [__METHOD__, $config['dsn']];
            if ($pool === null) {
                $config = array_merge($sharedConfig, $config);
                if ($cache instanceof CacheInterface && $cache->get($key)) {
                    // should not try this dead server now
                    continue;
                }
                $pool = $this->createPool($config);
                $poolStorage->add($config['dsn'], $pool);
            }

            try {
                $connection = $pool->borrow();
                $connection->poolId = $config['dsn'];
                $connection->open();

                return $connection;
            } catch (\Exception $e) {
                Yii::warning("Connection ({$config['dsn']}) failed: " . $e->getMessage(), __METHOD__);
                if ($cache instanceof CacheInterface) {
                    // mark this server as dead and only retry it after the specified interval
                    $cache->set($key, 1, $this->serverRetryInterval);
                }
            }
        }

        return null;
    }

    /**
     * @return StoredConnection
     * @throws BorrowConnectionTimeoutException
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    public function getMaster(): StoredConnection
    {
        $this->open();
        if ($this->singleMasterPool !== null) {
            return $this->singleMasterPool->borrow();
        }
//        $key = $this->shuffleMasters ? array_rand($this->masters) : array_key_first($this->masters);
        if ($this->shuffleMasters) {
            shuffle($this->masters);
        }
        return $this->openFromConnectionPool($this->mastersStorage, $this->masters, $this->masterConfig);
//        $key = 'master_' . $key;
//        $config = array_merge($this->masters[$key], $this->masterConfig);
//        $pool = $this->mastersStorage->get($key);
//        if ($pool === null) {
//            $pool = $this->createPool($config);
//            $this->mastersStorage->add($key, $pool);
//        }
//
//        /** @var StoredConnection $connection */
//        $connection = $pool->borrow();
//        $connection->poolId = $key;
//        $connection->open();
//
//        return $connection;
    }

    /**
     * @param bool $fallbackToMaster
     * @return \PDO|null
     * @throws BorrowConnectionTimeoutException
     * @throws yii\db\Exception
     */
    public function getSlavePdo($fallbackToMaster = true)
    {
        $db = $this->getSlave(false);
        if ($db === null) {
            return $fallbackToMaster ? $this->getMasterPdo() : null;
        }
        $db->open();

        return $db->pdo;
    }

    protected function createPool(array $config = null): ConnectionPool
    {
        $config = $config ?? [
                'dsn' => $this->dsn,
                'username' => $this->username,
                'password' => $this->password,
                'attributes' => $this->attributes,
                'charset' => $this->charset,
                'tablePrefix' => $this->tablePrefix,
                'enableSchemaCache' => $this->enableSchemaCache,
            ];

        $pool = new ConnectionPool([
            'minActive' => $config['idleNum'] ?? $this->idleNum,
            'maxActive' => $config['poolSize'] ?? $this->poolSize
        ], new PoolConnector($this), $config);
        $pool->init();

        return $pool;
    }

    public function open(): void
    {
        if ($this->singleMasterPool !== null) {
            return;
        }
        if (!empty($this->masters) && $this->mastersStorage === null) {
            $this->mastersStorage = new PoolStorage();
            return;
        }
        $pool = $this->createPool();
        $this->singleMasterPool = $pool;
    }

    /**
     * @return StoredConnection
     * @throws yii\db\Exception
     */
    private function getConnection(): StoredConnection
    {
        try {
            $connection = $this->transactionConnection ?? $this->getMaster();
        } catch (BorrowConnectionTimeoutException $exception) {
            throw new yii\db\Exception($exception->getMessage(), []);
        }

        return $connection;
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     * @throws yii\db\Exception
     */
    public function createCommand($sql = null, $params = []): Command
    {
        $config = ['class' => Command::class];
        $config['db'] = $this->getConnection();
        $config['sql'] = $sql;
        /** @var Command $command */
        $command = Yii::createObject($config);
        return $command->bindValues($params);
    }

    /**
     *{@inheritDoc}
     * @throws yii\db\Exception
     */
    public function beginTransaction($isolationLevel = null): ?Transaction
    {
        $connection = $this->getConnection();
        $connection->beginTransaction($isolationLevel);
        $this->transactionConnection = $connection;

        return $connection->transaction;
    }

    private $_schema;

    /**
     * @return yii\db\mysql\Schema
     * @throws BorrowConnectionTimeoutException
     * @throws InvalidConfigException
     */
    public function getSchema()
    {
        if ($this->_schema !== null) {
            return $this->_schema;
        }

        $config = [
            'class' => yii\db\mysql\Schema::class,
            'db' => $this->getMaster()
        ];

        return $this->_schema = Yii::createObject($config);
    }

    public function release(StoredConnection $connection): void
    {
        if ($connection !== $this->transactionConnection) {
            $this->singleMasterPool->return($connection);
        }
    }

    public function releaseMaster(StoredConnection $connection): void
    {
        if ($connection === $this->transactionConnection) {
            return;
        }
        if ($this->singleMasterPool !== null) {
            $this->singleMasterPool->return($connection);
        } elseif ($this->mastersStorage !== null) {
            $this->mastersStorage->get($connection->poolId)->return($connection);
        }
    }

    public function releaseSlave(StoredConnection $connection): void
    {
        if ($this->slavesStorage !== null) {
            $this->slavesStorage->get($connection->poolId)->return($connection);
        }
    }
}
