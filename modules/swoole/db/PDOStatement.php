<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Swoole\Coroutine\MySQL\Statement;
use yii\db\Exception;
use function is_string, is_int, is_object, is_array;

class PDOStatement extends \PDOStatement
{

    /**
     * @var Statement
     */
    public $statement;

    /**
     * @var float
     */
    public $timeout;

    /**
     * @var array
     */
    public $bindMap = [];

    /**
     * @var int
     */
    public $cursor = -1;

    /**
     * @var int
     */
    public $cursorOrientation = PDO::FETCH_ORI_NEXT;

    /**
     * @var int
     */
    public $fetchStyle = PDO::FETCH_BOTH;

    /**
     * @var array
     */
    private $resultSet = [];

    public function __construct(Statement $statement, array $bindMap = [])
    {
        $this->statement = $statement;
        $this->bindMap = $bindMap;
        $this->timeout = -1;
    }

    public function errorCode()
    {
        return $this->statement->errno;
    }

    public function errorInfo()
    {
        return $this->statement->error;
    }

    public function rowCount(): int
    {
        return $this->statement->affected_rows;
    }

    public function bindParam($parameter, &$variable, $type = null, $maxlen = null, $driverdata = null): bool
    {
        if (!is_string($parameter) && !is_int($parameter)) {
            return false;
        }

        $parameter = ltrim($parameter, ':');
        $this->bindMap[$parameter] = &$variable;

        return true;
    }

    public function bindValue($parameter, $variable, $type = null)
    {
        if (!is_string($parameter) && !is_int($parameter)) {
            return false;
        }

        if (is_object($variable)) {
            if (!method_exists($variable, '__toString')) {
                return false;
            }
            $variable = (string)$variable;
        }

        $parameter = ltrim($parameter, ':');
        $this->bindMap[$parameter] = $variable;

        return true;
    }

    private function afterExecute(): void
    {
        $this->cursor = -1;
        $this->bindMap = [];
    }

    /**
     * @param array $inputParameters
     * @return bool
     * @throws Exception
     */
    public function execute($inputParameters = [])
    {
        if (!empty($inputParameters)) {
            foreach ($inputParameters as $key => $value) {
                $this->bindParam($key, $value);
            }
        }
        $result = $this->statement->execute($this->bindMap, $this->timeout);
        if (!$result) {
            throw new Exception($this->statement->error, [], $this->statement->errno);
        }
        $this->afterExecute();

        return $result;
    }

    public function setFetchMode($fetchStyle, $className = null, $ctorargs = null)
    {
        $this->fetchStyle = $fetchStyle;
    }

    private function transBoth($rawData): array
    {
        $temp = [];
        foreach ($rawData as $row) {
            $rowSet = [];
            $i = 0;
            foreach ($row as $key => $value) {
                $rowSet[$key] = $value;
                $rowSet[$i++] = $value;
            }
            $temp[] = $rowSet;
        }

        return $temp;
    }

    private function transStyle(
        $rawData,
        $fetchStyle = null,
        $fetchArgument = null,
        $ctorArgs = null
    )
    {
        if (!is_array($rawData)) {
            return false;
        }
        if (empty($rawData)) {
            return $rawData;
        }

        $fetchStyle = $fetchStyle ?? $this->fetchStyle;
        $ctorArgs = $ctorArgs ?? [];

        $resultSet = [];
        switch ($fetchStyle) {
            case PDO::FETCH_BOTH:
                $resultSet = $this->transBoth($rawData);
                break;
            case PDO::FETCH_COLUMN:
                $resultSet = array_column(
                    is_numeric($fetchArgument) ? $this->transBoth($rawData) : $rawData,
                    $fetchArgument
                );
                break;
            case PDO::FETCH_OBJ:
                foreach ($rawData as $row) {
                    $resultSet[] = (object)$row;
                }
                break;
            case PDO::FETCH_NUM:
                foreach ($rawData as $row) {
                    $resultSet[] = array_values($row);
                }
                break;
            case PDO::FETCH_ASSOC:
            default:
                return $rawData;
        }

        return $resultSet;
    }

    public function fetch(
        $fetchStyle = null,
        $cursorOrientation = PDO::FETCH_ORI_NEXT,
        $cursorOffset = 0
    )
    {
        $this->fetchData();
        $cursorOrientation = $cursorOrientation ?? PDO::FETCH_ORI_NEXT;
        $cursorOffset = $cursorOffset ?? 0;

        switch ($cursorOrientation) {
            case PDO::FETCH_ORI_ABS:
                $this->cursor = $cursorOffset;
                break;
            case PDO::FETCH_ORI_REL:
                $this->cursor += $cursorOffset;
                break;
            case PDO::FETCH_ORI_NEXT:
            default:
                $this->cursor++;
        }

        if (isset($this->resultSet[$this->cursor])) {
            $result = $this->resultSet[$this->cursor];
            unset($this->resultSet[$this->cursor]);
        } else {
            $result = false;
        }

        if (empty($result)) {
            return $result;
        }

        return $this->transStyle([$result], $fetchStyle)[0];
    }

    public function fetchColumn($columnNumber = 0)
    {
        $columnNumber = $columnNumber ?? 0;

        return $this->fetch(PDO::FETCH_NUM)[$columnNumber];
    }

    public function fetchAll($fetchStyle = null, $fetchArgument = null, $ctorArgs = null)
    {
        $this->fetchData();
        return $this->transStyle($this->resultSet, $fetchStyle);
    }

    private function fetchData()
    {
        if (empty($this->resultSet)) {
            $this->resultSet = $this->statement->fetchAll();
            $this->statement->close();
        }
    }

    public function closeCursor()
    {
        $this->resultSet = [];
        $this->cursor = -1;
    }
}
