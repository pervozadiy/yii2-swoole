<?php

declare(strict_types=1);

namespace app\modules\swoole\db;

use Smf\ConnectionPool\ConnectionPool;

class PoolStorage
{
    private $pools = [];

    /**
     * Add a connection pool
     * @param string $key
     * @param ConnectionPool $pool
     */
    public function add(string $key, ConnectionPool $pool): void
    {
        $this->pools[$key] = $pool;
    }

    /**
     * Get a connection pool by key
     * @param string $key
     * @return ConnectionPool|null
     */
    public function get(string $key): ?ConnectionPool
    {
        return $this->pools[$key] ?? null;
    }

    /**
     * Close the connection by key
     * @param string $key
     * @return bool
     */
    public function close(string $key): bool
    {
        if (isset($this->pools[$key])) {
            return $this->pools[$key]->close();
        }

        return false;
    }

    /**
     * Close all connection pools
     */
    public function closeAll(): void
    {
        foreach ($this->pools as $pool) {
            $pool->close();
        }
    }
}
