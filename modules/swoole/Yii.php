<?php

declare(strict_types=1);

use app\modules\swoole\di\ContextContainer;

require dirname(dirname(__DIR__)) . '/vendor/yiisoft/yii2/BaseYii.php';

/**
 * Class Yii
 *
 * @property \app\modules\swoole\web\Application $app
 * @property ContextContainer $container
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var \app\modules\swoole\coroutine\Context
     */
    public static $context;

    /**
     * (@inheritdoc}
     */
//    public static function autoload($className): void
//    {
//        static::setAlias('@app', dirname(dirname(__DIR__)));
//        parent::autoload($className);
//    }
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = require dirname(dirname(__DIR__)) . '/vendor/yiisoft/yii2/classes.php';
Yii::$container = new yii\di\Container();