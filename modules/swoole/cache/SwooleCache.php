<?php

namespace app\modules\swoole\cache;

use Swoole\Table;
use yii\caching\Cache;

class SwooleCache extends \deepziyu\yii\swoole\cache\SwooleCache
{
    public $cacheSize = 4096;

    public $valueLength = 16384;

    private function getTable(): Table
    {
        $storage = \Yii::$app->server->getTableStorage();
        if ($table = $storage->get('cache')) {
            return $table;
        }
        $table = self::initCacheTable($this->cacheSize, $this->valueLength);
        $storage->add('cache', $table);

        return $table;
    }

    public function init()
    {
        $this->tableInstance = &$this->getTable();
        Cache::init();
    }

    protected function setValue($key, $value, $duration){
        $this->gc();
        $expire = ($duration === 0 ? $this->maxLive : $duration) + time();
        $valueLength = \strlen($value);
        return (boolean) $this->setValueRec($key, $value, $expire, $valueLength);
    }

    protected function setValueRec($key, &$value, $expire, $valueLength, $num = 0){
        $start = $num*static::$dataLength;
        if($start > $valueLength){
            return '';
        }
        $nextNum = $num+1;
        $nextId = $this->setValueRec($key, $value, $expire, $valueLength, $nextNum);
        if($nextId === false){
            return false;
        }
        if($num){
            $setKey = $key.$num;
        }else{
            $setKey = $key;
        }
        $result = $this->tableInstance->set($setKey,[
            'expire' => $expire,
            'nextId' => $nextId,
            'data' => substr($value,$start,static::$dataLength)
        ]);
        if($result === false){
            if($nextId){
                $this->deleteValue($nextId);
            }
            return false;
        }
        return $setKey;
    }
}
