<?php

namespace app\modules\swoole\cache;

use Swoole\Coroutine;
use Swoole\Serialize;
use yii\helpers\FileHelper;

class FileCache extends \yii\caching\FileCache
{
    public $serializer = [[Serialize::class, 'pack'], [Serialize::class, 'unpack']];

    /**
     * {@inheritdoc}
     */
    protected function getValue($key, bool $duplicated = false)
    {
        $cacheFile = $this->getCacheFile($key);
        if (@filemtime($cacheFile) > time()) {
            return Coroutine::readFile($cacheFile);
        }

        return false;
    }
    /**
     * {@inheritdoc}
     */
    protected function setValue($key, $value, $duration)
    {
        $this->gc();
        $cacheFile = $this->getCacheFile($key);
        if ($this->directoryLevel > 0) {
            @FileHelper::createDirectory(dirname($cacheFile), $this->dirMode, true);
        }
        // If ownership differs the touch call will fail, so we try to
        // rebuild the file from scratch by deleting it first
        // https://github.com/yiisoft/yii2/pull/16120
        if (is_file($cacheFile) && function_exists('posix_geteuid') && fileowner($cacheFile) !== posix_geteuid()) {
            @unlink($cacheFile);
        }
        try {
            Coroutine::writeFile($cacheFile, $value);
            if ($this->fileMode !== null) {
                @chmod($cacheFile, $this->fileMode);
            }
            if ($duration <= 0) {
                $duration = 31536000; // 1 year
            }

            return @touch($cacheFile, $duration + time());
        } catch (\Throwable $e) {
            \Yii::warning("Unable to write cache file '{$cacheFile}': {$e->getMessage()}", __METHOD__);

            return false;
        }
    }
}
