<?php

namespace app\modules\swoole\task;

use Swoole\Serialize;
use yii\queue\JobInterface;
use yii\queue\serializers\SerializerInterface;

class Serializer implements SerializerInterface
{

    /**
     * @param JobInterface|mixed $job
     * @return string
     */
    public function serialize($job)
    {
        return Serialize::pack($job);
    }

    /**
     * @param string $serialized
     * @return JobInterface
     */
    public function unserialize($serialized)
    {
        return Serialize::unpack($serialized);
    }
}
