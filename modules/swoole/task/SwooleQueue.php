<?php

declare(strict_types=1);

namespace app\modules\swoole\task;

use app\modules\swoole\server\Manager;
use Swoole\Http\Server;
use Swoole\Table;
use yii\base\InvalidArgumentException;
use yii\base\NotSupportedException;
use yii\queue\Queue;

class SwooleQueue extends Queue
{
    public $tableSize = 1024;
    /**
     * @var array
     */
    private $status = [];

    /**
     * @var Manager
     */
    private $server;

    /**
     * @var Table
     */
    private $taskMap;

    public $serializer = Serializer::class;

    public function __construct(Manager $server, $config = [])
    {
        $this->server = $server;
        $this->taskMap = $this->getTable();

        parent::__construct($config);
    }

    protected function getTable(): Table
    {
        if (null === $this->taskMap) {
            $table = new Table($this->tableSize);
            $table->column('id', Table::TYPE_STRING, 255);
            $table->column('ttr', Table::TYPE_INT);
            $table->column('attempt', Table::TYPE_INT);
            $table->create();
            $this->taskMap = $table;
        }

        return $this->taskMap;
    }

    /**
     * @param string $message
     * @param int $ttr time to reserve in seconds
     * @param int $delay
     * @param mixed $priority
     * @return string id of a job message
     * @throws NotSupportedException
     */
    protected function pushMessage($message, $ttr, $delay, $priority)
    {
        if ($priority !== null) {
            throw new NotSupportedException('Job priority is not supported in the driver.');
        }
        $id = uniqid('async_task_', true);
        $this->status[$id] = self::STATUS_WAITING;
        if ($delay) {
            $this->server->server->after($delay * 1000, function () use ($message, $ttr, $id) {
                $this->status[$id] = self::STATUS_RESERVED;
                $taskId = $this->server->server->task($message);
                $this->taskMap->set('swoole_task_' . $taskId, [
                    'id' => $id,
                    'ttr' => $ttr,
                    'attempt' => 0
                ]);
            });
        } else {
            $this->status[$id] = self::STATUS_RESERVED;
            $taskId = $this->server->server->task($message);
            $this->taskMap->set('swoole_task_' . $taskId, [
                'id' => $id,
                'ttr' => $ttr,
                'attempt' => 0
            ]);
        }

        return $id;
    }

    public function processTask(Server $server, int $taskId, int $fromId, $message): void
    {
        $key = 'swoole_task_' . $taskId;
        do {
            $this->taskMap->incr($key, 'attempt');
            /** @var Table\Row $data */
            $data = $this->taskMap->get($key);
        } while (!$this->handleMessage($data['id'], $message, $data['ttr'], $data['attempt']));

        $this->taskMap->del($key);
        $server->finish($data->value);
    }

    public function taskFinish(Server $server, int $taskId, $data): void
    {
        $this->status[$data['id']] = self::STATUS_DONE;
    }

    /**
     * @param string $id of a job message
     * @return int status code
     */
    public function status($id): int
    {
        if (isset($this->status[$id])) {
            return $this->status[$id];
        }

        throw new InvalidArgumentException('Job with id=' . $id . ' not found!');
    }
}
