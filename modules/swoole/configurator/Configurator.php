<?php

declare(strict_types=1);

namespace app\modules\swoole\configurator;

use app\modules\swoole\server\ServerConfig;
use Swoole\Http\Server;
use yii\base\InvalidConfigException;

class Configurator
{
    /**
     * @var ServerConfig
     */
    private $config;

    public function __construct(ServerConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param Server $server
     * @throws InvalidConfigException
     */
    public function configure(Server $server): void
    {
        if ($this->config->validate()) {
            $server->set($this->config->attributes);
        } else {
            //ToDo create custom exception to throw validation errors
            \Yii::error($this->config->errors, __METHOD__);
            throw new InvalidConfigException('Invalid server config!');
        }
    }
}
