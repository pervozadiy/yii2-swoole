<?php

return [
    'singletons' => [
        \app\modules\swoole\server\Manager::class => \app\modules\swoole\server\Manager::class,
        \app\modules\swoole\bootstrap\BootstrapManager::class => [
            [],
            //ToDo add bootstrapper list to custom configuration
            [
                [
                    \app\modules\swoole\bootstrap\WebBootstrapper::class,
                    \app\modules\swoole\bootstrap\HmrBootstrapper::class
                ]
            ]
        ]
    ],
    'definitions' => []
];