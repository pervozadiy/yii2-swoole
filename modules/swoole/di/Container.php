<?php

declare(strict_types=1);

namespace app\modules\swoole\di;

use app\modules\swoole\db\ConnectionManager;
use app\modules\swoole\web\Request;
use app\modules\swoole\web\Response;
use ReflectionClass;
use yii\di\NotInstantiableException;
use function call_user_func_array;
use function count;

class Container extends \yii\di\Container
{
    /**
     * the original classes will be replace
     * @var array
     */
    private static $classCompatible = [
        'yii\web\Request' => Request::class,
        'yii\web\Response' => Response::class,
        'yii\web\ErrorHandler' => 'deepziyu\yii\swoole\web\ErrorHandler',
        'yii\log\Dispatcher' => 'deepziyu\yii\swoole\log\Dispatcher',
        'yii\log\FileTarget' => 'deepziyu\yii\swoole\log\FileTarget',
        'yii\log\Logger' => 'deepziyu\yii\swoole\log\Logger',
        'yii\db\Connection' => ConnectionManager::class,
    ];

    private $cc;

    public function __construct(ContextContainer $contextContainer, $config = [])
    {
        $this->cc = $contextContainer;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    protected function build($class, $params, $config)
    {
        if (isset(self::$classCompatible[$class])) {
            $class = self::$classCompatible[$class];
        } elseif (isset(self::$classCompatible[trim($class, "\\")])) {
            $class = self::$classCompatible[trim($class, "\\")];
        }

        if (null === $obj = $this->cc->getPersistent($class, $params, $config)) {
            $obj = $this->buildSafe($class, $params, $config);
        }
        return $obj;
    }

    /**
     * build classes secure in Coroutine
     *
     * @param $class
     * @param $params
     * @param $config
     * @return \object
     * @throws NotInstantiableException
     * @throws \yii\base\InvalidConfigException
     */
    private function buildSafe($class, $params, $config)
    {
        /* @var $reflection ReflectionClass */
        [$reflection, $dependencies] = $this->getDependencies($class);

        foreach ($params as $index => $param) {
            $dependencies[$index] = $param;
        }

        $dependencies = $this->resolveDependencies($dependencies, $reflection);
        if (!$reflection->isInstantiable()) {
            throw new NotInstantiableException($reflection->name);
        }
        if (empty($config)) {
            return $reflection->newInstanceArgs($dependencies);
        }

        if (!empty($dependencies) && $reflection->implementsInterface('yii\base\Configurable')) {
            // set $config as the last parameter (existing one will be overwritten)
            $dependencies[count($dependencies) - 1] = $config;
            $instance = $reflection->newInstanceWithoutConstructor();
            call_user_func_array([$instance, '__construct'], $dependencies);
            return $instance;
        }
        $object = $reflection->newInstanceWithoutConstructor();
        call_user_func_array([$object, '__construct'], $dependencies);
        foreach ($config as $name => $value) {
            $object->$name = $value;
        }
        return $object;
    }
}