<?php

namespace app\modules\swoole\di;

use app\modules\swoole\db\ConnectionManager;
use app\modules\swoole\web\Request;
use app\modules\swoole\web\Response;
use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;

class ContextContainer extends \yii\di\Container
{
    private $_persistentSingletons = [];
    /**
     * the original classes will be replace
     * @var array
     */
    protected static $classCompatible = [
        'yii\web\Request' => Request::class,
        'yii\web\Response' => Response::class,
        'yii\web\ErrorHandler' => 'deepziyu\yii\swoole\web\ErrorHandler',
        'yii\log\Dispatcher' => 'deepziyu\yii\swoole\log\Dispatcher',
        'yii\log\FileTarget' => 'deepziyu\yii\swoole\log\FileTarget',
        'yii\log\Logger' => 'deepziyu\yii\swoole\log\Logger',
        'yii\db\Connection' => ConnectionManager::class,
//        'yii\db\Connection' => \deepziyu\yii\swoole\db\Connection::class,
    ];

    /**
     * These classes will be kept in worker memory persistently
     * @var array
     */
    protected static $_persistent = [
        'deepziyu\yii\swoole\pool\MysqlPool',
        ConnectionManager::class,
        'deepziyu\yii\swoole\pool\RedisPool',
        'deepziyu\yii\swoole\web\ErrorHandler',
        'yii\web\UrlManager',
        'yii\i18n\I18N',
        'yii\caching\ArrayCache',
    ];

    /**
     * These classes will be rebuilt on every single request
     * @var array
     */
    protected static $_disposable = [];

    public $persistent = [];

    public $disposable = [];

    public function init()
    {
        $this->persistent = array_merge(self::$_persistent, $this->persistent);
        $this->disposable = array_merge(self::$_disposable, $this->disposable);
    }

    public function get($class, $params = [], $config = [])
    {
        if (isset(self::$classCompatible[$class])) {
            $class = self::$classCompatible[$class];
        } elseif (isset(self::$classCompatible[trim($class, "\\")])) {
            $class = self::$classCompatible[trim($class, "\\")];
        }
        if ($this->isPersistent($class)) {
            return $this->_persistentSingletons[$class] ?? parent::get($class, $params, $config);
        }

        return $this->getContainer()->get($class, $params, $config);
    }

    public function addPersistent(string $class): void
    {
        $this->disposable = array_diff($this->disposable, [$class]);
        $this->persistent[] = $class;
    }

    public function addDisposable(string $class): void
    {
        $this->persistent = array_diff($this->persistent, [$class]);
        $this->disposable[] = $class;
    }

    /**
     * @return Container
     * @throws InvalidArgumentException
     */
    protected function getContainer(): Container
    {
        if (null === $container = Yii::$context->getContainer()) {
            $container = new Container($this);
            Yii::$context->setContainer($container);
        }

        return $container;
    }

    public function __get($name)
    {
        return $this->getContainer()->{$name};
    }

    public function __call($name, $arguments)
    {
        $container = $this->getContainer();

        return call_user_func_array([$container, $name], $arguments);
    }

    public function setPersistent($class, $definition = [], array $params = []): void
    {
        $this->addPersistent($class);
        $this->setSingleton($class, $definition, $params);
    }

    public function setPersistentObj($class, $object): void
    {
        $this->addPersistent($class);
        $this->_persistentSingletons[$class] = $object;
    }

    /**
     * @param string $class
     * @param array $params
     * @param array $config
     * @return object|null
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function getPersistent($class, $params, $config)
    {
        if ($this->isPersistent($class)) {
            return $this->get($class, $params, $config);
        }

        return null;
    }

    /**
     * @param $class
     * @return bool
     */
    public function isPersistent($class): bool
    {
        return !in_array($class, $this->disposable, true) && in_array($class, $this->persistent, true);
    }

    /**
     * @param string $class
     * @param array $params
     * @param array $config
     * @return string
     */
    protected function buildKey($class, $params = [], $config = []): string
    {
        return $class . md5(Json::encode($params) . Json::encode($config));
    }
}
