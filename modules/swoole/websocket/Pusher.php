<?php

namespace app\modules\swoole\websocket;

use app\modules\swoole\websocket\socketIo\Broadcast;
use Swoole\Websocket\Server;
use function in_array;

/**
 * Class Pusher
 */
class Pusher
{
    /**
     * @var Server
     */
    protected $server;

    /**
     * @var int
     */
    protected $opcode;

    /**
     * @var int
     */
    protected $sender;

    /**
     * @var array
     */
    protected $descriptors;

    /**
     * @var bool
     */
    protected $broadcast;

    /**
     * @var string
     */
    protected $event;

    /**
     * @var mixed|null
     */
    protected $message;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * Push constructor.
     *
     * @param int $opcode
     * @param int $sender
     * @param array $descriptors
     * @param bool $broadcast
     * @param string $event
     * @param mixed|null $message
     * @param Server $server
     * @param Parser $parser
     */
    protected function __construct(
        int $opcode,
        int $sender,
        array $descriptors,
        bool $broadcast,
        Server $server,
        Parser $parser
    )
    {
        $this->opcode = $opcode;
        $this->sender = $sender;
        $this->descriptors = $descriptors;
        $this->broadcast = $broadcast;
        $this->server = $server;
        $this->parser = $parser;
    }

    /**
     * Static constructor
     *
     * @param array $data
     * @param Server $server
     *
     * @return Pusher
     * @throws \yii\base\InvalidConfigException
     */
    public static function make(array $data, Server $server): Pusher
    {
        /** @var Broadcast $broadcast */
        $broadcast = \Yii::createObject(Broadcast::class);
        /** @var Parser $parser */
        $parser = \Yii::createObject(Parser::class);
        $fds = $broadcast->mapSids($data['sids'] ?? []);

        $sender = $data['sender'] ? $broadcast->getFd($data['sender']) : 0;

        return new static(
            $data['opcode'] ?? 1,
            $sender ?? 0,
            $fds,
            $data['broadcast'] ?? false,
            $server,
            $parser
        );
    }

    /**
     * @param int $descriptor
     *
     * @return self
     */
    public function addDescriptor($descriptor): self
    {
        return $this->addDescriptors([$descriptor]);
    }

    /**
     * @param array $descriptors
     *
     * @return self
     */
    public function addDescriptors(array $descriptors): self
    {
        $this->descriptors = array_values(
            array_unique(
                array_merge($this->descriptors, $descriptors)
            )
        );

        return $this;
    }

    /**
     * @param int $descriptor
     *
     * @return bool
     */
    public function hasDescriptor(int $descriptor): bool
    {
        return in_array($descriptor, $this->descriptors, true);
    }

    /**
     * @return Server
     */
    public function getServer(): Server
    {
        return $this->server;
    }

    /**
     * Push message to related descriptors
     *
     * @param mixed $payload
     *
     * @return void
     */
    public function push($payload): void
    {
        $payload = $this->parser->encode($payload);

        // check if to broadcast to other clients
        if ($this->broadcast) {
            foreach ($this->server->connections as $fd) {
                if ($this->sender !== $fd && $this->server->isEstablished($fd)) {
                    $this->server->push($fd, $payload, $this->opcode);
                }
            }
            return;
        }

        // attach sender if not broadcast
        if ($this->sender && !$this->hasDescriptor($this->sender)) {
            $this->addDescriptor($this->sender);
        }
        // push message to designated fds
        foreach ($this->descriptors as $fd) {
            if ($this->server->isEstablished($fd)) {
                $this->server->push($fd, $payload, $this->opcode);
            }
        }
    }
}
