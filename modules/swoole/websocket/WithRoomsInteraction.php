<?php


namespace app\modules\swoole\websocket;


use function array_diff;
use function array_filter;
use function array_unique;
use function array_values;
use function array_merge;

trait WithRoomsInteraction
{
    /**
     * @var RoomStorage
     */
    protected $roomStorage;

    protected $to = [];

    protected function createRoomStorage(): RoomStorage
    {
        $storage = new RoomStorage([
            'room_rows' => 4096,
            'room_size' => 2048,
            'client_rows' => 8192,
            'client_size' => 2048,
        ]);
        $storage->prepare();

        return $storage;
    }

    public function to(string ...$rooms)
    {
        foreach ($rooms as $room) {
            if (!in_array($room, $this->to, true)) {
                $this->to[] = $room;
            }
        }
        return $this;
    }

    public function in(string ...$rooms)
    {
        return $this->to(...$rooms);
    }

    public function getSids(): array
    {
        $sids = [];

        foreach ($this->to as $room) {
            $clients = $this->roomStorage->getClients($room);
            // fallback sid with wrong type back to sids array
            if (empty($clients) && is_string($room)) {
                $sids[] = $room;
            } else {
                $sids = array_merge($sids, $clients);
            }
        }
        $this->to = [];

        return array_values(array_unique($sids));
    }

    public function join(...$rooms): self
    {
        $this->roomStorage->add($this->sid, ...$rooms);

        return $this;
    }

    public function leave(...$rooms): self
    {
        $this->roomStorage->delete($this->sid, ...$rooms);

        return $this;
    }

}
