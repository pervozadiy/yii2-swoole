<?php

namespace app\modules\swoole\websocket;

use Swoole\Table;
use SwooleTW\Http\Websocket\Rooms\RoomContract;
use function array_diff;
use function array_values;
use function count;
use function in_array;
use function is_array;

class RoomStorage
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @var \Swoole\Table
     */
    protected $rooms;

    /**
     * @var \Swoole\Table
     */
    protected $sids;

    /**
     * TableRoom constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Do some init stuffs before workers started.
     *
     * @return RoomStorage
     */
    public function prepare(): self
    {
        $this->initRoomsTable();
        $this->initSidsTable();

        return $this;
    }

    /**
     * Add a socket sid to multiple rooms.
     *
     * @param string $sid
     * @param string ...$roomNames
     */
    public function add(string $sid, string ...$roomNames): void
    {
        $rooms = $this->getRooms($sid);

        foreach ($roomNames as $room) {
            $sids = $this->getClients($room);

            if (in_array($sid, $sids, true)) {
                continue;
            }

            $sids[] = $sid;
            $rooms[] = $room;

            $this->setClients($room, $sids);
        }

        $this->setRooms($sid, $rooms);
    }

    /**
     * Delete a socket sid from multiple rooms.
     *
     * @param string $sid
     * @param string ...$roomNames
     */
    public function delete(string $sid, string ...$roomNames): void
    {
        $allRooms = $this->getRooms($sid);
        $rooms = count($roomNames) ? $roomNames : $allRooms;

        $removeRooms = [];
        foreach ($rooms as $room) {
            $sids = $this->getClients($room);

            if (!in_array($sid, $sids, true)) {
                continue;
            }

            $this->setClients($room, array_values(array_diff($sids, [$sid])));
            $removeRooms[] = $room;
        }

        $this->setRooms($sid, array_values(array_diff($allRooms, $removeRooms)));
    }

    /**
     * Get all sockets by a room key.
     *
     * @param string room
     *
     * @return array
     */
    public function getClients(string $room): array
    {
        return $this->getValue($room, RoomContract::ROOMS_KEY) ?? [];
    }

    /**
     * Get all rooms by a sid.
     *
     * @param string $sid
     * @return array
     */
    public function getRooms(string $sid): array
    {
        return $this->getValue($sid, 'sids') ?? [];
    }

    protected function setClients(string $room, array $sids): RoomStorage
    {
        return $this->setValue($room, $sids, RoomContract::ROOMS_KEY);
    }


    protected function setRooms(string $sids, array $rooms): RoomStorage
    {
        return $this->setValue($sids, $rooms, 'sids');
    }

    /**
     * Init rooms table
     */
    protected function initRoomsTable(): void
    {
        $this->rooms = new Table($this->config['room_rows']);
        $this->rooms->column('value', Table::TYPE_STRING, $this->config['room_size']);
        $this->rooms->create();
    }

    /**
     * Init descriptors table
     */
    protected function initSidsTable(): void
    {
        $this->sids = new Table($this->config['client_rows']);
        $this->sids->column('value', Table::TYPE_STRING, $this->config['client_size']);
        $this->sids->create();
    }

    /**
     * Set value to table
     *
     * @param $key
     * @param array $value
     * @param string $table
     *
     * @return $this
     */
    public function setValue($key, array $value, string $table): self
    {
        $this->checkTable($table);

        $this->$table->set($key, ['value' => json_encode($value)]);

        return $this;
    }

    /**
     * Get value from table
     *
     * @param string $key
     * @param string $table
     *
     * @return array|mixed
     */
    public function getValue(string $key, string $table)
    {
        $this->checkTable($table);

        $value = $this->$table->get($key);

        return $value ? json_decode($value['value'], true) : [];
    }

    /**
     * Check table for exists
     *
     * @param string $table
     */
    protected function checkTable(string $table): void
    {
        if (!property_exists($this, $table) || !$this->$table instanceof Table) {
            throw new \InvalidArgumentException("Invalid table name: `{$table}`.");
        }
    }
}
