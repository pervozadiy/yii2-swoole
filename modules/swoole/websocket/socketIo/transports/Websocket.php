<?php

namespace app\modules\swoole\websocket\socketIo\transports;

use app\modules\swoole\websocket\Parser;
use app\modules\swoole\websocket\socketIo\middlewares\Heartbeat;
use app\modules\swoole\websocket\socketIo\middlewares\NspConnection;
use app\modules\swoole\websocket\socketIo\Packet;
use app\modules\swoole\websocket\socketIo\Transport;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

class Websocket extends Transport
{
    protected $middlewares = [
        NspConnection::class,
        Heartbeat::class,
    ];

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            'open',
            'message',
            'close',
        ];
    }

    public function onOpen(Server $server, Request $request): bool
    {
        $sid = $this->assignSocket($server, $request);
        if ($sid === null) {
            return false;
        }
        $this->broadcast->connect($server, $sid, $request->fd);
        $this->broadcast->reset();

        return true;
    }

    private function assignSocket(Server $server, Request $request): ?string
    {
        if (!isset($request->get['sid'])) {
            $sid = base64_encode(uniqid());
            $payload = json_encode([
                'sid' => $sid,
                'upgrades' => ['websocket'],
                'pingInterval' => $this->pingInterval,
                'pingTimeout' => $this->pingTimeout,
            ]);
            $initPayload = Packet::OPEN . $payload;
            $connectPayload = Packet::MESSAGE . Packet::CONNECT;

            $server->push($request->fd, $initPayload);
            $server->push($request->fd, $connectPayload);

            return $sid;
        }
        return null;
    }

    public function onMessage(Server $server, Frame $frame): bool
    {
        foreach ($this->middlewares as $middlewareConfig) {
            $middleware = \Yii::createObject($middlewareConfig);
            if ($middleware->handle($server, $frame)) {
                return true;
            }
        }
        /** @var Parser $parser */
        $parser = \Yii::createObject(Parser::class);
        ['event' => $event, 'data' => $data, 'nsp' => $nsp] = $parser->decode($frame);
        $sid = $this->broadcast->getSid($frame->fd);
        $socket = $this->broadcast->of($nsp)->getSocket($sid);
        $socket && $event && $socket->call($event, $data);
        return true;
    }

    public function onClose(Server $server, int $fd, int $reactorId): bool
    {
        $sid = $this->broadcast->getSid($fd);
        if ($sid === null) {
            return false;
        }
        $socket = $this->broadcast->getClient($sid);
        if ($socket === null) {
            return false;
        }
        $socket->call('disconnect');
        // leave all rooms
        $socket->leave();
        // disconnect from namespace and close connection
        $socket->disconnect(true);

        return true;
    }
}
