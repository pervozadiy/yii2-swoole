<?php

namespace app\modules\swoole\websocket\socketIo\transports;

use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\middlewares\ConnectionUpgrade;
use app\modules\swoole\websocket\socketIo\middlewares\NspConnection;
use app\modules\swoole\websocket\socketIo\Packet;
use app\modules\swoole\websocket\socketIo\Transport;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
use yii\base\BaseObject;

class Polling extends Transport
{
    protected $middlewares = [
        ConnectionUpgrade::class,
        NspConnection::class
    ];

    /**
     * @inheritDoc
     */
    public function events(): array
    {
        return [
            'open',
            'message',
            'request'
        ];
    }

    public function onOpen(Server $server, Request $request): bool
    {
        if (!isset($request->get['sid'])) {
            return false;
        }
        $sid = (string)$request->get['sid'];
        $this->broadcast->getFd($sid) === null && $this->broadcast->bind($sid, $request->fd);

        return true;
    }

    public function onRequest(Request $request, Response $response): bool
    {
        if (rtrim($request->server['request_uri'], '/') !== $this->path || $request->get['transport'] !== 'polling') {
            return false;
        }
        $response->header('Content-Type', 'text/plain; charset=UTF-8');
        $response->header('strict-transport-security', 'max-age=63072000');
        $response->status(200);
        if ($this->assignSocket($request, $response)) {
            return true;
        }
        $method = strtoupper($request->server['request_method']) ?? 'GET';
        if ($method === 'GET') {
            $response->end('1:' . Packet::NOOP);
            return true;
        }
        if ($method === 'OPTIONS') {
            $response->header('Access-Control-Allow-Headers', 'Content-Type');
            $response->end();
            return true;
        }
        if ($method === 'POST' && ($sid = $request->get['sid'])) {
            foreach (Packet::parseInput($request->rawContent()) as $packet) {
                $this->broadcast->addPacketToQueue($sid, $packet);
            }
            $response->end('2:ok');
            return true;
        }
        return false;
    }

    private function assignSocket(Request $request, Response $response): ?string
    {
        if ($request->get['sid'] === null) {
            $sid = base64_encode(uniqid());
            $packet = Packet::OPEN . json_encode([
                    'sid' => $sid,
                    'upgrades' => ['websocket'],
                    'pingInterval' => $this->pingInterval,
                    'pingTimeout' => $this->pingTimeout
                ]);

            $response->end(strlen($packet) . ':' . $packet . '2:' . Packet::MESSAGE . Packet::CONNECT);

            return $sid;
        }

        return null;
    }

    public function onMessage(Server $server, Frame $frame): bool
    {
        foreach ($this->middlewares as $middlewareConfig) {
            $middleware = \Yii::createObject($middlewareConfig);
            if ($middleware->handle($server, $frame)) {
                return true;
            }
        }
        return false;
    }
}
