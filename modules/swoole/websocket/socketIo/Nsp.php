<?php

namespace app\modules\swoole\websocket\socketIo;

use app\modules\swoole\websocket\Pusher;
use app\modules\swoole\websocket\WithRoomsInteraction;
use Swoole\WebSocket\Server;
use Yii;

class Nsp
{
    use WithRoomsInteraction {
        getSids as getSidsFromRooms;
    }

    /**
     * @var int
     */
    private $sender;

    /**
     * @var string
     */
    private $nsp;

    /**
     * @var callable[]
     */
    private $callbacks = [];

    /**
     * @var Server
     */
    private $server;

    /**
     * @var Socket[]
     */
    private $sockets = [];

    /**
     * @var Broadcast
     */
    private $broadcast;

    /**
     * @var Socket
     */
    private $socket;

    public function __construct(Broadcast $broadcast, string $nsp = '/')
    {
        $this->broadcast = $broadcast;
        $this->nsp = $nsp;
    }

    public function getFd(string $sid): ?int
    {
        return $this->broadcast->getFd($sid);
    }

    public function getServer(): ?Server
    {
        return $this->server;
    }


    public function onConnection(callable $callback): Nsp
    {
        $this->callbacks[] = $callback;

        return $this;
    }

    /**
     * @param Server $server
     * @param int $fd
     * @param string $sid
     * @internal
     */
    public function connect(Server $server, string $sid, int $fd = null): void
    {
        $this->server = $server;
        $this->sender = $fd;
        $this->sockets[$sid] = new Socket($this, $sid, $fd);
        $fd !== null && $this->broadcast->bind($sid, $fd);
        foreach ($this->callbacks as $i => $callback) {
            Yii::$context->createChild($callback, $this->sockets[$sid]);
        }
    }

    public function getSocket(string $sid): ?Socket
    {
        return $this->sockets[$sid] ?? null;
    }


    public function isConnected(string $sid): bool
    {
        return isset($this->sockets[$sid]);
    }

    public function emit(string $event, $data): bool
    {
        if ($this->server === null) {
            return false;
        }
        $sids = $this->getSids();
        $assigned = !empty($this->to);

        // if no sids are found, but rooms are assigned
        // that means trying to emit to a non-existing room
        // skip it directly instead of pushing to a task queue
        if (empty($sids) && $assigned) {
            return false;
        }

        $payload = [
            'sender' => $this->sender,
            'sids' => $sids,
            'broadcast' => false,
        ];

        $result = true;
        $pusher = Pusher::make($payload, $this->server);
        $pusher->push(['nsp' => $this->nsp, 'event' => $event, 'data' => $data]);

        return $result !== false;
    }

    protected function getSids(): array
    {
        if ($this->to) {
            return $this->getSidsFromRooms();
        }
        return array_unique(array_keys($this->sockets));
    }

    public function getClients(): array
    {
        return $this->sockets;
    }

    public function getClient(string $sid): ?Socket
    {
        return $this->sockets[$sid] ?? null;
    }

    public function delete(string $sid): void
    {
        unset($this->sockets[$sid]);
    }

    public function __toString(): string
    {
       return $this->nsp;
    }

    public function reset(): void
    {
        $this->callbacks = [];
    }

    public function close(int $fd = null): bool
    {
        return $this->server->close($fd ?? $this->sender);
    }
}
