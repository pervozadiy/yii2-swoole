<?php

namespace app\modules\swoole\websocket\socketIo;

use app\modules\swoole\websocket\Pusher;
use app\modules\swoole\websocket\WithRoomsInteraction;

class Socket
{
    use WithRoomsInteraction;

    public $nsp;

    private $callbacks = [];

    private $isBroadcast = false;

    private $sid;

    private $fd;

    public $name;

    public $connected;

    public function __construct(Nsp $nsp, string $sid, ?int $fd = null)
    {
        $this->nsp = $nsp;
        $this->sid = $sid;
        $this->fd = $fd;
        $this->roomStorage = $this->createRoomStorage();
    }

    public function getFd(): ?int
    {
        return $this->fd;
    }

    public function getId(): string
    {
        return $this->sid;
    }

    public function emit(string $event, $data): bool
    {
        $server = $this->nsp->getServer();
        if ($server === null) {
            return false;
        }
        $sids = $this->getSids();
        $assigned = !empty($this->to);

        // if no fds are found, but rooms are assigned
        // that means trying to emit to a non-existing room
        // skip it directly instead of pushing to a task queue
        if (empty($sids) && $assigned) {
            return false;
        }

        $payload = [
            'sender' => $this->sid,
            'sids' => $sids,
            'broadcast' => $this->isBroadcast,
        ];

        $result = true;
        $pusher = Pusher::make($payload, $server);
        $pusher->push(['nsp' => (string)$this->nsp, 'event' => $event, 'data' => $data]);

        return $result !== false;
    }

    public function on(string $event, callable $callback): self
    {
        $this->callbacks[$event] = $callback;

        return $this;
    }

    public function call(string $event, $data = null)
    {
        if (!$this->eventExists($event)) {
            return null;
        }

        // dispatch request to pipeline if middleware are set
//        if ($isConnect && count($this->middleware)) {
//            $data = $this->setRequestThroughMiddleware($data);
//        }

        return call_user_func($this->callbacks[$event], $this, $data);
    }

    public function eventExists(string $event): bool
    {
        return array_key_exists($event, $this->callbacks);
    }

    public function close(): bool
    {
        if ($this->fd === null) {
            return false;
        }
        return $this->nsp->close($this->fd);
    }

    public function disconnect(bool $close = false): void
    {
        $this->nsp->delete($this->sid);
        $close && $this->close();
    }

    public function broadcast($flag = true): Socket
    {
        $this->isBroadcast = $flag;

        return $this;
    }
}
