<?php

namespace app\modules\swoole\websocket\socketIo;

use yii\base\BaseObject;
use function in_array;

abstract class Transport extends BaseObject
{
    /**
     * @var Broadcast
     */
    protected $broadcast;

    public $pingInterval = 25000;

    public $pingTimeout = 60000;

    public $path = '/socket.io';

    public function __construct(Broadcast $broadcast, array $config = [])
    {
        $this->broadcast = $broadcast;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    abstract public function events(): array;

    public function isApplicable(string $event): bool
    {
        return in_array($event, $this->events(), true);
    }

    public function handle(string $event, array $args): bool
    {
        $method = 'on' . ucfirst($event);
        if (method_exists($this, $method)) {
            return (bool)$this->$method(...$args);
        }
        return false;
    }
}
