<?php

namespace app\modules\swoole\websocket\socketIo;

use Swoole\WebSocket\Server;

/**
 * Class Broadcast
 * @package app\modules\swoole\websocket\socketIo
 *
 * @method Nsp onConnection(callable $callback)
 * @method Socket|null getSocket(string $sid)
 * @method bool isConnected()
 * @method Nsp to(...$room)
 * @method Nsp in(...$room)
 * @method Nsp emit(string $event, $data)
 * @method Socket[] getClients()
 * @method void connect(Server $server, string $sid, int $fd = null)
 * @method void reset()
 */
class Broadcast
{
    /**
     * @var Nsp[]
     */
    private $nsps = [];

    private $sidToFdMap = [];

    private $packetQueue = [];

    public function addPacketToQueue(string $sid, string $packet): Broadcast
    {
        $this->packetQueue[$sid][] = $packet;

        return $this;
    }

    public function flushPacketsQueue(): array
    {
        $packets = $this->packetQueue;
        $this->packetQueue = [];

        return $packets;
    }

    public function bind(string $sid, int $fd): void
    {
        $this->sidToFdMap[$sid] = $fd;
    }

    public function getFd(string $sid): ?int
    {
        return $this->sidToFdMap[$sid] ?? null;
    }

    public function getSid(int $fd): ?string
    {
        if (false === $key = array_search($fd, $this->sidToFdMap, true)) {
            return null;
        }
        return $key;
    }

    public function mapSids(array $sids): array
    {
        return array_filter(array_map(function (string $sid) {
            return $this->sidToFdMap[$sid] ?? null;
        }, $sids));
    }

    public function of(string $nsp): Nsp
    {
        if (!isset($this->nsps[$nsp])) {
            $this->nsps[$nsp] = new Nsp($this, $nsp);
        }

        return $this->nsps[$nsp];
    }

    public function getClient(string $sid): ?Socket
    {
        foreach ($this->nsps as $nsp) {
            if ($client = $nsp->getClient($sid)) {
                return $client;
            }
        }

        return null;
    }

    /**
     * @return Socket[]
     */
    public function getAllClients(): array
    {
        $clients = [];
        foreach ($this->nsps as $nsp) {
            array_merge($clients, $nsp->getClients());
        }

        return $clients;
    }

    public function __call($name, $arguments)
    {
        return $this->of('/')->$name(...$arguments);
    }

}
