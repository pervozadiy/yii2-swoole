<?php

namespace app\modules\swoole\websocket\socketIo\middlewares;

use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\Packet;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

class ConnectionUpgrade
{
    /**
     * @var Broadcast
     */
    private $broadcast;

    public function __construct(Broadcast $broadcast)
    {
        $this->broadcast = $broadcast;
    }

    public function handle(Server $server, Frame $frame): bool
    {
        if (Packet::getSocketType($frame->data) !== Packet::UPGRADE) {
            return false;
        }
        $fd = $frame->fd;
        $sid = $this->broadcast->getSid($fd);
        $this->broadcast->connect($server, $sid, $fd);
        $this->broadcast->reset();
        foreach ($this->broadcast->flushPacketsQueue() as $sid => $packets) {
            foreach ($packets as $packet) {
                $nsp = Packet::getNsp($packet);
                if (Packet::getEngineType($packet) === Packet::CONNECT) {
                    $fd = $this->broadcast->getFd($sid);
                    if ($fd !== null) {
                        $this->broadcast->of($nsp)->connect($server, $sid, $fd);
                        $this->broadcast->of($nsp)->reset();
                        $connectPayload = Packet::MESSAGE . Packet::CONNECT . $nsp;
                        $server->push($fd, $connectPayload);
                    }
                    continue;
                }
                if ($payload = Packet::getPayload($packet)) {
                    ['event' => $event, 'data' => $data] = $payload;
                    $socket = $this->broadcast->of($nsp)->getSocket($sid);
                    $socket && $event && $socket->call($event, $data);
                    continue;
                }
            }

        }

        return true;
    }
}
