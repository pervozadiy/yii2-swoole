<?php

namespace app\modules\swoole\websocket\socketIo\middlewares;

use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\Packet;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

class NspConnection
{
    /**
     * @var Broadcast
     */
    private $broadcast;

    public function __construct(Broadcast $broadcast)
    {
        $this->broadcast = $broadcast;
    }

    public function handle(Server $server, Frame $frame): bool
    {
        if (Packet::getEngineType($frame->data) === Packet::CONNECT) {
            $nsp = Packet::getNsp($frame->data);
            $this->broadcast->of($nsp)->connect($server, $frame->fd);
            $this->broadcast->of($nsp)->reset();
            $connectPayload = Packet::MESSAGE . Packet::CONNECT . $nsp;
            $server->push($frame->fd, $connectPayload);
            return true;
        }

        return false;
    }
}
