<?php


namespace app\modules\swoole\websocket\socketIo\middlewares;

use app\modules\swoole\websocket\socketIo\Packet;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

class Heartbeat
{
    /**
     * If return value is true will skip decoding.
     *
     * @param Server $server
     * @param Frame $frame
     *
     * @return boolean
     */
    public function handle(Server $server, Frame $frame)
    {
        $packet = $frame->data;
        $packetLength = strlen($packet);
        $payload = '';

        if ($p = Packet::getPayload($packet)) {
            return false;
        }

        if ($isPing = Packet::isSocketType($packet, 'ping')) {
            $payload .= Packet::PONG;
            if ($packetLength > 1) {
                $payload .= substr($packet, 1, $packetLength - 1);
            }
            $server->push($frame->fd, $payload);

            return true;
        }

        return false;
    }
}
