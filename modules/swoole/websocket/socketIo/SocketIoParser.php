<?php

namespace app\modules\swoole\websocket\socketIo;

use app\modules\swoole\websocket\Parser;
use app\modules\swoole\websocket\socketIo\middlewares\Heartbeat;
use app\modules\swoole\websocket\socketIo\middlewares\NspConnection;
use Swoole\WebSocket\Frame;
use Swoole\Websocket\Server;

class SocketIoParser implements Parser
{
    protected $middlewares = [
        NspConnection::class,
        Heartbeat::class,
    ];

    public function encode(array $message): string
    {
        $nsp = $message['nsp'] ?? '/';
        $event = $message['event'] ?? '';
        $data = $message['data'] ?? null;
        $packet = Packet::MESSAGE . Packet::EVENT;
        if ($nsp !== '/') {
            $packet .= $nsp . ',';
        }
        if ($data) {
            $shouldEncode = is_array($data) || is_object($data);
            $data = $shouldEncode ? json_encode($data) : $data;
            $format = $shouldEncode ? '["%s",%s]' : '["%s","%s"]';
            $payload = sprintf($format, $event, $data);
        } else {
            $payload = sprintf('["%s"]', $event);
        }

        return $packet . $payload;
    }

    public function decode(Frame $frame): array
    {
        $payload = Packet::getPayload($frame->data);
        $nsp = Packet::getNsp($frame->data);

        return [
            'event' => $payload['event'] ?? null,
            'data' => $payload['data'] ?? null,
            'nsp' => $nsp
        ];
    }

    /**
     * Executes all middlewares
     *
     * @param Server $server
     * @param Frame $frame
     * @return bool TRUE if any middleware was applied, FALSE otherwise
     * @throws \yii\base\InvalidConfigException
     */
    public function execute(Server $server, Frame $frame): bool
    {
        foreach ($this->middlewares as $middlewareConfig) {
            $middleware = \Yii::createObject($middlewareConfig);
            if ($middleware->handle($server, $frame)) {
                return true;
            }
        }

        return false;
    }
}
