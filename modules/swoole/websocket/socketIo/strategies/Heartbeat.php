<?php

namespace app\modules\swoole\websocket\socketIo\strategies;

use app\modules\swoole\websocket\socketIo\Packet;

class Heartbeat
{
    public function handle(string $packet): ?string
    {
        $packetLength = strlen($packet);
        $payload = '';

        if ($p = Packet::getPayload($packet)) {
            return null;
        }

        if ($isPing = Packet::isSocketType($packet, 'ping')) {
            $payload .= Packet::PONG;
            if ($packetLength > 1) {
                $payload .= substr($packet, 1, $packetLength - 1);
            }

            return $payload;
        }

        return null;
    }
}
