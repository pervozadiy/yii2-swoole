<?php

namespace app\modules\swoole\websocket\socketIo\strategies;

use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\Packet;

class NspConnection
{
    /**
     * @var Broadcast
     */
    private $broadcast;

    public function __construct(Broadcast $broadcast)
    {
        $this->broadcast = $broadcast;
    }

    public function handle(string $packet): bool
    {
        if (Packet::getEngineType($packet) === Packet::CONNECT) {
            $nsp = Packet::getNsp($packet);
            $this->broadcast->of($nsp)->connect($server, $frame->fd);
            $this->broadcast->of($nsp)->reset();
            $connectPayload = Packet::MESSAGE . Packet::CONNECT . $nsp;
            $server->push($frame->fd, $connectPayload);
            return true;
        }

        return false;
    }
}
