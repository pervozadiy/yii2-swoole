<?php

namespace app\modules\swoole\websocket\socketIo;

/**
 * Class Packet
 */
class Packet
{
    public const GENERAL_PATTERN = '/\d+:(?<packet>\d{2}.*?)/';

    public const DETAILED_PATTERN = '/(?<length>\d+):(?<code>\d{2})\[\"(?<payload>.*?)\"]/';

    /**
     * Socket.io packet type `open`.
     */
    public const OPEN = 0;

    /**
     * Socket.io packet type `close`.
     */
    public const CLOSE = 1;

    /**
     * Socket.io packet type `ping`.
     */
    public const PING = 2;

    /**
     * Socket.io packet type `pong`.
     */
    public const PONG = 3;

    /**
     * Socket.io packet type `message`.
     */
    public const MESSAGE = 4;

    /**
     * Socket.io packet type 'upgrade'
     */
    public const UPGRADE = 5;

    /**
     * Socket.io packet type `noop`.
     */
    public const NOOP = 6;

    /**
     * Engine.io packet type `connect`.
     */
    public const CONNECT = 0;

    /**
     * Engine.io packet type `disconnect`.
     */
    public const DISCONNECT = 1;

    /**
     * Engine.io packet type `event`.
     */
    public const EVENT = 2;

    /**
     * Engine.io packet type `ack`.
     */
    public const ACK = 3;

    /**
     * Engine.io packet type `error`.
     */
    public const ERROR = 4;

    /**
     * Engine.io packet type 'binary event'
     */
    public const BINARY_EVENT = 5;

    /**
     * Engine.io packet type `binary ack`. For acks with binary arguments.
     */
    public const BINARY_ACK = 6;

    /**
     * Socket.io packet types.
     */
    public static $socketTypes = [
        0 => 'OPEN',
        1 => 'CLOSE',
        2 => 'PING',
        3 => 'PONG',
        4 => 'MESSAGE',
        5 => 'UPGRADE',
        6 => 'NOOP',
    ];

    /**
     * Engine.io packet types.
     */
    public static $engineTypes = [
        0 => 'CONNECT',
        1 => 'DISCONNECT',
        2 => 'EVENT',
        3 => 'ACK',
        4 => 'ERROR',
        5 => 'BINARY_EVENT',
        6 => 'BINARY_ACK',
    ];

    /**
     * Get socket packet type of a raw payload.
     *
     * @param string $packet
     *
     * @return int|null
     */
    public static function getSocketType(string $packet): ?int
    {
        $type = $packet[0] ?? null;

        if (! array_key_exists($type, static::$socketTypes)) {
            return null;
        }

        return (int) $type;
    }

    public static function getEngineType(string $packet): ?int
    {
        $type = $packet[1] ?? null;

        if (! array_key_exists($type, static::$engineTypes)) {
            return null;
        }

        return (int) $type;
    }

    /**
     * Get data packet from a raw payload.
     *
     * @param string $packet
     *
     * @return array|null
     */
    public static function getPayload(string $packet): ?array
    {
        $packet = trim($packet);
        $start = strpos($packet, '[');

        if ($start === false || substr($packet, -1) !== ']') {
            return null;
        }

        $data = substr($packet, $start, strlen($packet) - $start);
        $data = json_decode($data, true);

        if ($data === null) {
            return null;
        }

        return [
            'event' => $data[0],
            'data' => $data[1] ?? null,
        ];
    }

    /**
     * Return if a socket packet belongs to specific type.
     *
     * @param $packet
     * @param string $typeName
     *
     * @return bool
     */
    public static function isSocketType($packet, string $typeName): bool
    {
        $type = array_search(strtoupper($typeName), static::$socketTypes, true);

        if ($type === false) {
            return false;
        }

        return static::getSocketType($packet) === $type;
    }

    public static function getNsp(string $packet): string
    {
        $packet = trim($packet);

        return strtok(strstr($packet, '/'), ',') ?: '/';
    }

    /**
     * Retruns array of packets
     * @param string $input
     * @return array
     */
    public static function parseInput(string $input): array
    {
        $packets = [];
        while (preg_match('/(?<length>\d+):/', $input, $matches)) {
            $pos = strpos($input, $matches[0]);
            if ($pos !== false) {
                $input = substr_replace($input, '', $pos, strlen($matches[0]));
            }
            $packets[] = substr($input, 0, $matches['length']);
        }

        return $packets;
    }
}