<?php

namespace app\modules\swoole\websocket;

use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

interface Parser
{
    public function encode(array $payload);

    public function decode(Frame $frame);

    public function execute(Server $server, Frame $frame);
}
