FROM php:7.3-fpm

ARG HOST_UID=${HOST_UID}

VOLUME ["/var/www"]

RUN apt-get update && apt-get install -y \
        procps \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        openssl \
        libssh-dev \
        libpcre3 \
        libpcre3-dev \
        libnghttp2-dev \
        libhiredis-dev \
        zlib1g-dev \
        libicu-dev \
        libpq-dev \
        vim \
        git \
        bzip2 \
        unzip \
        sudo \
        wget \
        gdb \
        sendmail \
        libzip-dev \
        default-mysql-client && \
        apt autoremove && apt clean

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install \
        intl \
        zip \
        exif \
        mbstring \
        pcntl \
        iconv \
        gd \
        pdo_mysql \
        mysqli \
        json \
        opcache \
        sockets

# enable opcache
#RUN echo "opcache.enable_cli=1" >> /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini

#install redis
RUN pecl install redis && docker-php-ext-enable redis

#install inotify
RUN pecl install inotify && docker-php-ext-enable inotify

# install swoole
RUN cd /tmp && \
    git clone https://github.com/swoole/swoole-src.git && cd swoole-src && \
    git checkout tags/v4.4.7 && \
    phpize && \
    ./configure \
    --enable-openssl  \
    --enable-http2  \
    --enable-async-redis \
    --enable-mysqlnd && \
    make && make install && \
    docker-php-ext-enable swoole && \
    echo "swoole.fast_serialize=On" >> /usr/local/etc/php/conf.d/docker-php-ext-swoole-serialize.ini && \
    rm -rf /tmp/*

#install swoole async ext
RUN cd /tmp && \
    git clone https://github.com/swoole/async-ext.git && cd async-ext && \
#    git checkout tags/v4.3.3 && \
    phpize && \
    ./configure && \
    make -j 4 && \
    make install && \
    docker-php-ext-enable swoole_async && \
    rm -rf /tmp/*

#install swoole serialize ext
RUN cd /tmp && \
    git clone https://github.com/swoole/ext-serialize.git && cd ext-serialize && \
    phpize && \
    ./configure && \
    make -j 4 && \
    make install && \
    docker-php-ext-enable swoole_serialize && \
    rm -rf /tmp/*

RUN mkdir -p /var/www/data

# install composer
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN set -x && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer global require "fxp/composer-asset-plugin:^1.4.0" \
    && composer global require hirak/prestissimo --prefer-dist --no-interaction

RUN usermod -u ${HOST_UID} -d /var/www -s /bin/bash -p secret www-data \
    && groupmod -g ${HOST_UID} www-data \
    && chown -R www-data:www-data /var/www

WORKDIR /var/www

EXPOSE 8101

USER ${HOST_UID}

#ENTRYPOINT ["./yii", "swoole/start"]
#ENTRYPOINT ["./start.sh"]
#CMD ["start"]
