<?php

namespace app\controllers;

use app\jobs\TestJob;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\SignupForm;
use app\modules\swoole\websocket\socketIo\Broadcast;
use app\modules\swoole\websocket\socketIo\Socket;
use Swoole\Coroutine;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        ini_set('max_execution_time', 0);
//        Yii::$context->createChild(function () {
//            $cli = new \Co\Http\Client('reqres.in', 443, true);
//            $cli->set([
//                'timeout' => -1,
//                'ssl_host_name' => 'reqres.in',
//            ]);
//            $cli->get('/api/users?page=2&delay=1');
//        });
        /** @var Broadcast $handler */
        $handler = Yii::$container->get(Broadcast::class);
        $handler->onConnection(static function (Socket $socket) use ($handler) {
            var_dump('aaaaaaaaaaaaaaaaaaa');
            $socket->on('eeeeee', static function () use ($socket, $handler) {
                $socket->broadcast()->emit('news', ['text' => 'sam ti eeee']);
            });
        });

        $handler->of('/nsp')->onConnection(static function (Socket $socket) {
            var_dump('bbbbbbbbbbbbbbbbbb');
            $socket->on('eeeeee', static function () use ($socket) {
                $socket->emit('news', ['text' => 'sam ti bbb']);
            });
        });

//        $handler->on('JOIN_ROOM', function (WebSocket $handler, $data) {
//            var_dump($data);
//            $handler->join('test');
//            $handler->to('test')->emit('message', 'to roommates');
//
//        });
//        for ($i = 0; $i < 100; $i++) {
//            Yii::$app->queue->push(new TestJob());
//        }

        Yii::$app->cache->set('pidor', 'ajhaa');

        return $this->render('index');
    }

    private function generateRandomString($length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = \strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function generator(int $number)
    {
        for ($i = 0; $i < $number; $i++) {
            yield $this->generateRandomString(1000);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $client = new Coroutine\MySQL();
        $client->connect([
            'host' => 'master',
            'port' => 3306,
            'user' => 'hushchyn',
            'password' => 'secret',
            'database' => 'yii2basic',
            'charset' => 'utf8mb4',
            'strict_type' => true,
            'timeout' => -1,
        ]);

        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
