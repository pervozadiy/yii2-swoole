<?php

namespace app\helpers;

use deepziyu\yii\swoole\coroutine\Context;

class CoroutineHelper
{
    /**
     * Create child coroutine in case to use parent`s context
     * @param \Closure $callback
     * @return mixed
     */
    public static function createChild(\Closure $callback)
    {
        $puid = Context::getcoroutine();
        return \Swoole\Coroutine::create(function () use ($puid,$callback){
            Context::markParent($puid);
            $callback();
        });
    }
}
