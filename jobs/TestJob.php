<?php

namespace app\jobs;

use app\models\User;
use yii\queue\JobInterface;
use yii\queue\Queue;
use yii\queue\RetryableJobInterface;

class TestJob implements RetryableJobInterface
{

    /**
     * @param Queue $queue which pushed and is handling the job
     * @return void|mixed result of the job execution
     */
    public function execute($queue)
    {
//        throw new \RuntimeException();
//        sleep(2);
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 100; $i++) {
            $user = new User();
            $user->username = uniqid($faker->userName, true);
            $user->email = uniqid($faker->email, true);
            $user->password_hash = \Yii::$app->security->generateRandomString(16);
            $user->auth_key = \Yii::$app->security->generateRandomString(16);
            $res = $user->insert(false);
        }

        return 'ahahahaha';
    }

    /**
     * @return int time to reserve in seconds
     */
    public function getTtr()
    {
        return 1;
    }

    /**
     * @param int $attempt number
     * @param \Exception|\Throwable $error from last execute of the job
     * @return bool
     */
    public function canRetry($attempt, $error)
    {
        if ($error instanceof \RuntimeException) {
            return $attempt <= 3;
        }

        return false;
    }
}
