#!/usr/bin/env bash
set -x

pid=0

# SIGTERM-handler
term_handler() {
  echo "$pid";
  ps aux;
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handlerps
trap 'kill ${!}; term_handler' SIGTERM
trap 'kill ${!}; term_handler' SIGINT

# run application
./yii swoole/start &
pid="$!"

# wait forever
while true
do
  tail -f /dev/null & wait ${!}
done